package mortezaqn

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.mortezaqn.samplescannerurovo.R
import mortezaqn.scanner.BarcodeReceiver
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    private var barcodeReceiver: BarcodeReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tvResult = findViewById<TextView>(R.id.tv_result)

        barcodeReceiver = BarcodeReceiver(this, lifecycle) { value ->
            Timber.i("result => $value")
            tvResult.text = value
        }
    }
}