package mortezaqn.common

import android.device.scanner.configuration.PropertyID

object Constants {

    private const val KEY_AZTEC_ENABLE = "AZTEC_ENABLE"
    val KEY_ARRAY_AZTEC = arrayOf(KEY_AZTEC_ENABLE)
    val INT_ARRAY_AZTEC = intArrayOf(PropertyID.AZTEC_ENABLE)

    private const val KEY_C25_ENABLE = "C25_ENABLE"
    val KEY_ARRAY_CHINESE25 = arrayOf(KEY_C25_ENABLE)
    val INT_ARRAY_CHINESE25 = intArrayOf(PropertyID.C25_ENABLE)

    private const val KEY_CODABAR_ENABLE = "CODABAR_ENABLE"
    private const val KEY_CODABAR_LENGTH1 = "CODABAR_LENGTH1"
    private const val KEY_CODABAR_LENGTH2 = "CODABAR_LENGTH2"
    private const val KEY_CODABAR_NOTIS = "CODABAR_NOTIS"
    private const val KEY_CODABAR_CLSI = "CODABAR_CLSI"
    val KEY_ARRAY_CODABAR = arrayOf(
        KEY_CODABAR_ENABLE,
        KEY_CODABAR_LENGTH1,
        KEY_CODABAR_LENGTH2,
        KEY_CODABAR_NOTIS,
        KEY_CODABAR_CLSI,
    )
    val INT_ARRAY_CODABAR = intArrayOf(
        PropertyID.CODABAR_ENABLE,
        PropertyID.CODABAR_LENGTH1,
        PropertyID.CODABAR_LENGTH2,
        PropertyID.CODABAR_NOTIS,
        PropertyID.CODABAR_CLSI,
    )

    private const val KEY_CODE32_ENABLE = "CODE32_ENABLE"
    val KEY_ARRAY_CODE32 = arrayOf(KEY_CODE32_ENABLE)
    val INT_ARRAY_CODE32 = intArrayOf(PropertyID.CODE32_ENABLE)

    val KEY_ARRAY_CODE39 = arrayOf(
        "CODE39_ENABLE",
        "CODE39_LENGTH1",
        "CODE39_LENGTH2",
        "CODE39_ENABLE_CHECK",
        "CODE39_SEND_CHECK",
        "CODE39_FULL_ASCII",
    )
    val INT_ARRAY_CODE39 = intArrayOf(
        PropertyID.CODE39_ENABLE,
        PropertyID.CODE39_LENGTH1,
        PropertyID.CODE39_LENGTH2,
        PropertyID.CODE39_ENABLE_CHECK,
        PropertyID.CODE39_SEND_CHECK,
        PropertyID.CODE39_FULL_ASCII,
    )

    val KEY_ARRAY_CODE93 = arrayOf(
        "CODE93_ENABLE",
        "CODE93_LENGTH1",
        "CODE93_LENGTH2",
    )
    val INT_ARRAY_CODE93 = intArrayOf(
        PropertyID.CODE93_ENABLE,
        PropertyID.CODE93_LENGTH1,
        PropertyID.CODE93_LENGTH2,
    )

    val KEY_ARRAY_CODE128 = arrayOf(
        "CODE128_ENABLE",
        "CODE128_LENGTH1",
        "CODE128_LENGTH2",
        "CODE128_CHECK_ISBT_TABLE",
    )
    val INT_ARRAY_CODE128 = intArrayOf(
        PropertyID.CODE128_ENABLE,
        PropertyID.CODE128_LENGTH1,
        PropertyID.CODE128_LENGTH2,
        PropertyID.CODE128_CHECK_ISBT_TABLE,
    )

    val INT_ARRAY_COMPOSITE_CC_AB = intArrayOf(PropertyID.COMPOSITE_CC_AB_ENABLE)
    val KEY_ARRAY_COMPOSITE_CC_AB = arrayOf("COMPOSITE_CC_AB_ENABLE")

    val INT_ARRAY_COMPOSITE_CC_C = intArrayOf(PropertyID.COMPOSITE_CC_C_ENABLE)
    val KEY_ARRAY_COMPOSITE_CC_C = arrayOf("COMPOSITE_CC_C_ENABLE")

    val INT_ARRAY_DATAMATRIX = intArrayOf(PropertyID.DATAMATRIX_ENABLE)
    val KEY_ARRAY_DATAMATRIX = arrayOf("DATAMATRIX_ENABLE")

    val INT_ARRAY_DISCRETE25 = intArrayOf(PropertyID.D25_ENABLE)
    val KEY_ARRAY_DISCRETE25 = arrayOf("D25_ENABLE")

    val INT_ARRAY_EAN8 = intArrayOf(PropertyID.EAN8_ENABLE)
    val KEY_ARRAY_EAN8 = arrayOf("EAN8_ENABLE")

    val INT_ARRAY_EAN13 = intArrayOf(PropertyID.EAN13_ENABLE, PropertyID.EAN13_BOOKLANDEAN)
    val KEY_ARRAY_EAN13 = arrayOf("EAN13_ENABLE", "EAN13_BOOKLANDEAN")

    val INT_ARRAY_GS1_14 = intArrayOf(PropertyID.GS1_14_ENABLE)
    val KEY_ARRAY_GS1_14 = arrayOf("GS1_14_ENABLE")

    val INT_ARRAY_GS1_128 = intArrayOf(PropertyID.CODE128_GS1_ENABLE)
    val KEY_ARRAY_GS1_128 = arrayOf("CODE128_GS1_ENABLE")
    val INT_ARRAY_GS1_EXP = intArrayOf(
        PropertyID.GS1_EXP_ENABLE,
        PropertyID.GS1_EXP_LENGTH1,
        PropertyID.GS1_EXP_LENGTH2,
    )
    val KEY_ARRAY_GS1_EXP = arrayOf("GS1_EXP_ENABLE", "GS1_EXP_LENGTH1", "GS1_EXP_LENGTH2")
    val INT_ARRAY_GS1_LIMIT = intArrayOf(PropertyID.GS1_LIMIT_ENABLE)
    val KEY_ARRAY_GS1_LIMIT = arrayOf("GS1_LIMIT_ENABLE")
    val INT_ARRAY_INTERLEAVED25 = intArrayOf(
        PropertyID.I25_ENABLE,
        PropertyID.I25_LENGTH1,
        PropertyID.I25_LENGTH2,
        PropertyID.I25_ENABLE_CHECK,
        PropertyID.I25_SEND_CHECK,
    )
    val KEY_ARRAY_INTERLEAVED25 = arrayOf(
        "I25_ENABLE",
        "I25_LENGTH1",
        "I25_LENGTH2",
        "I25_ENABLE_CHECK",
        "I25_SEND_CHECK",
    )
    val INT_ARRAY_MATRIX25 = intArrayOf(PropertyID.M25_ENABLE)
    val KEY_ARRAY_MATRIX25 = arrayOf("M25_ENABLE")
    val INT_ARRAY_MAXICODE = intArrayOf(PropertyID.MAXICODE_ENABLE)
    val KEY_ARRAY_MAXICODE = arrayOf("MAXICODE_ENABLE")
    val INT_ARRAY_MICROPDF417 = intArrayOf(PropertyID.MICROPDF417_ENABLE)
    val KEY_ARRAY_MICROPDF417 = arrayOf("MICROPDF417_ENABLE")
    val INT_ARRAY_MSI = intArrayOf(
        PropertyID.MSI_ENABLE,
        PropertyID.MSI_LENGTH1,
        PropertyID.MSI_LENGTH2,
        PropertyID.MSI_REQUIRE_2_CHECK,
        PropertyID.MSI_SEND_CHECK,
        PropertyID.MSI_CHECK_2_MOD_11,
    )
    val KEY_ARRAY_MSI = arrayOf(
        "MSI_ENABLE",
        "MSI_LENGTH1",
        "MSI_LENGTH2",
        "MSI_REQUIRE_2_CHECK",
        "MSI_SEND_CHECK",
        "MSI_CHECK_2_MOD_11",
    )
    val INT_ARRAY_PDF417 = intArrayOf(PropertyID.PDF417_ENABLE)
    val KEY_ARRAY_PDF417 = arrayOf("PDF417_ENABLE")
    val INT_ARRAY_QRCODE = intArrayOf(PropertyID.QRCODE_ENABLE)
    val KEY_ARRAY_QRCODE = arrayOf("QRCODE_ENABLE")
    val INT_ARRAY_TRIOPTIC = intArrayOf(PropertyID.TRIOPTIC_ENABLE)
    val KEY_ARRAY_TRIOPTIC = arrayOf("TRIOPTIC_ENABLE")
    val INT_ARRAY_UPCA = intArrayOf(
        PropertyID.UPCA_ENABLE,
        PropertyID.UPCA_SEND_CHECK,
        PropertyID.UPCA_SEND_SYS,
        PropertyID.UPCA_TO_EAN13,
    )
    val KEY_ARRAY_UPCA = arrayOf(
        "UPCA_ENABLE",
        "UPCA_SEND_CHECK",
        "UPCA_SEND_SYS",
        "UPCA_TO_EAN13",
    )
    val INT_ARRAY_UPCE = intArrayOf(
        PropertyID.UPCE_ENABLE,
        PropertyID.UPCE_SEND_CHECK,
        PropertyID.UPCE_SEND_SYS,
        PropertyID.UPCE_TO_UPCA,
    )
    val KEY_ARRAY_UPCE = arrayOf(
        "UPCE_ENABLE",
        "UPCE_SEND_CHECK",
        "UPCE_SEND_SYS",
        "UPCE_TO_UPCA",
    )
    val INT_ARRAY_UPCE1 = intArrayOf(PropertyID.UPCE1_ENABLE)
    val KEY_ARRAY_UPCE1 = arrayOf("UPCE1_ENABLE")

    // private const val KEY_ = ""
    // private const val KEY_ = ""
    //
    // private const val KEY_ = ""
    // private const val KEY_ = ""
    //
    // val KEY_ARRAY_ = arrayOf()
    // val INT_ARRAY_ = intArrayOf()
}