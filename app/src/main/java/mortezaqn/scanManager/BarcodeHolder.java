package mortezaqn.scanManager;

import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;

class BarcodeHolder {
    CheckBoxPreference mBarcodeEnable = null;
    EditTextPreference mBarcodeLength1 = null;
    EditTextPreference mBarcodeLength2 = null;
    CheckBoxPreference mBarcodeNOTIS = null;
    CheckBoxPreference mBarcodeCLSI = null;
    CheckBoxPreference mBarcodeISBT = null;
    CheckBoxPreference mBarcodeChecksum = null;
    CheckBoxPreference mBarcodeSendCheck = null;
    CheckBoxPreference mBarcodeFullASCII = null;
    ListPreference mBarcodeCheckDigit = null;
    CheckBoxPreference mBarcodeBookland = null;
    CheckBoxPreference mBarcodeSecondChecksum = null;
    CheckBoxPreference mBarcodeSecondChecksumMode = null;
    ListPreference mBarcodePostalCode = null;
    CheckBoxPreference mBarcodeSystemDigit = null;
    CheckBoxPreference mBarcodeConvertEAN13 = null;
    CheckBoxPreference mBarcodeConvertUPCA = null;
    CheckBoxPreference mBarcodeEanble25DigitExtensions = null;
    CheckBoxPreference mBarcodeDPM = null;
    int[] mParaIds = null;
    String[] mParaKeys = null;
}