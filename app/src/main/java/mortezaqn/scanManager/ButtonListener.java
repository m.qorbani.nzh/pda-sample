package mortezaqn.scanManager;

import android.annotation.SuppressLint;
import android.device.scanner.configuration.Triggering;
import android.view.MotionEvent;
import android.view.View;

import com.mortezaqn.samplescannerurovo.R;

import timber.log.Timber;

class ButtonListener implements View.OnClickListener, View.OnTouchListener {

    private final ScanManagerDemo mScanDemo;

    public ButtonListener(ScanManagerDemo mScanDemo) {
        this.mScanDemo = mScanDemo;
    }

    public void onClick(View v) {
        Timber.d("onClick: ButtonListener onClick");
    }

    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouch(View v, MotionEvent event) {
        if (v.getId() == R.id.scan_trigger) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                Timber.d("onTouch button Up");
                //                                    mScan.setText(R.string.scan_trigger_start);
                if (mScanDemo.mScanManager.getTriggerMode() == Triggering.HOST) {
                    stopDecode();
                }
            }
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Timber.d("onTouch button Down");
                //                                    mScan.setText(R.string.scan_trigger_end);
                startDecode();
            }
        }
        return false;
    }

    private void startDecode() {
        if (!mScanDemo.isScanEnable()) {
            Timber.i("startDecode ignore, Scan enable:%s", mScanDemo.isScanEnable());
            return;
        }
        boolean lockState = mScanDemo.mScanManager.getTriggerLockState();
        if (lockState) {
            Timber.i("startDecode ignore, Scan lockTrigger state:%s", lockState);
            return;
        }
        if (mScanDemo.mScanManager != null) {
            mScanDemo.mScanManager.startDecode();
        }
    }

    private void stopDecode() {
        if (!mScanDemo.isScanEnable()) {
            Timber.i("stopDecode ignore, Scan enable:%s", mScanDemo.isScanEnable());
            return;
        }
        if (mScanDemo.mScanManager != null) {
            mScanDemo.mScanManager.stopDecode();
        }
    }
}