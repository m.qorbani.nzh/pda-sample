package mortezaqn.scanManager;

import android.device.scanner.configuration.PropertyID;

import java.util.HashMap;
import java.util.Map;

class Const {
    static final Map<Integer, String> PROPERTY = new HashMap<Integer, String>() {{

        put(PropertyID.AZTEC_ENABLE, "AZTEC_ENABLE");

        put(PropertyID.C25_ENABLE, "C25_ENABLE");

        put(PropertyID.CODABAR_ENABLE, "CODABAR_ENABLE");
        put(PropertyID.CODABAR_LENGTH1, "CODABAR_LENGTH1");
        put(PropertyID.CODABAR_LENGTH2, "CODABAR_LENGTH2");
        put(PropertyID.CODABAR_NOTIS, "CODABAR_NOTIS");
        put(PropertyID.CODABAR_CLSI, "CODABAR_CLSI");

        put(PropertyID.CODE11_ENABLE, "CODE11_ENABLE");
        put(PropertyID.CODE11_LENGTH2, "CODE11_LENGTH2");
        put(PropertyID.CODE11_LENGTH1, "CODE11_LENGTH1");
        put(PropertyID.CODE11_SEND_CHECK, "CODE11_SEND_CHECK");

        put(PropertyID.CODE32_ENABLE, "CODE32_ENABLE");

        put(PropertyID.CODE39_ENABLE, "CODE39_ENABLE");
        put(PropertyID.CODE39_LENGTH1, "CODE39_LENGTH1");
        put(PropertyID.CODE39_LENGTH2, "CODE39_LENGTH2");
        put(PropertyID.CODE39_ENABLE_CHECK, "CODE39_ENABLE_CHECK");
        put(PropertyID.CODE39_SEND_CHECK, "CODE39_SEND_CHECK");
        put(PropertyID.CODE39_FULL_ASCII, "CODE39_FULL_ASCII");

        put(PropertyID.CODE93_ENABLE, "CODE93_ENABLE");
        put(PropertyID.CODE93_LENGTH1, "CODE93_LENGTH1");
        put(PropertyID.CODE93_LENGTH2, "CODE93_LENGTH2");

        put(PropertyID.CODE128_ENABLE, "CODE128_ENABLE");
        put(PropertyID.CODE128_LENGTH1, "CODE128_LENGTH1");
        put(PropertyID.CODE128_LENGTH2, "CODE128_LENGTH2");
        put(PropertyID.CODE128_CHECK_ISBT_TABLE, "CODE128_CHECK_ISBT_TABLE");

        put(PropertyID.COMPOSITE_CC_AB_ENABLE, "COMPOSITE_CC_AB_ENABLE");

        put(PropertyID.COMPOSITE_CC_C_ENABLE, "COMPOSITE_CC_C_ENABLE");

        put(PropertyID.DATAMATRIX_ENABLE, "DATAMATRIX_ENABLE");

        put(PropertyID.D25_ENABLE, "D25_ENABLE");

        put(PropertyID.EAN8_ENABLE, "EAN8_ENABLE");

        put(PropertyID.EAN13_ENABLE, "EAN13_ENABLE");
        put(PropertyID.EAN13_BOOKLANDEAN, "EAN13_BOOKLANDEAN");

        put(PropertyID.GS1_14_ENABLE, "GS1_14_ENABLE");

        put(PropertyID.CODE128_GS1_ENABLE, "CODE128_GS1_ENABLE");

        put(PropertyID.GS1_EXP_ENABLE, "GS1_EXP_ENABLE");
        put(PropertyID.GS1_EXP_LENGTH1, "GS1_EXP_LENGTH1");
        put(PropertyID.GS1_EXP_LENGTH2, "GS1_EXP_LENGTH2");

        put(PropertyID.GS1_LIMIT_ENABLE, "GS1_LIMIT_ENABLE");

        put(PropertyID.I25_ENABLE, "I25_ENABLE");
        put(PropertyID.I25_LENGTH1, "I25_LENGTH1");
        put(PropertyID.I25_LENGTH2, "I25_LENGTH2");
        put(PropertyID.I25_ENABLE_CHECK, "I25_ENABLE_CHECK");
        put(PropertyID.I25_SEND_CHECK, "I25_SEND_CHECK");

        put(PropertyID.M25_ENABLE, "M25_ENABLE");

        put(PropertyID.MAXICODE_ENABLE, "MAXICODE_ENABLE");

        put(PropertyID.MICROPDF417_ENABLE, "MICROPDF417_ENABLE");
        put(PropertyID.MSI_ENABLE, "MSI_ENABLE");
        put(PropertyID.MSI_LENGTH1, "MSI_LENGTH1");
        put(PropertyID.MSI_LENGTH2, "MSI_LENGTH2");
        put(PropertyID.MSI_REQUIRE_2_CHECK, "MSI_REQUIRE_2_CHECK");
        put(PropertyID.MSI_SEND_CHECK, "MSI_SEND_CHECK");
        put(PropertyID.MSI_CHECK_2_MOD_11, "MSI_CHECK_2_MOD_11");

        put(PropertyID.PDF417_ENABLE, "PDF417_ENABLE");

        put(PropertyID.QRCODE_ENABLE, "QRCODE_ENABLE");

        put(PropertyID.TRIOPTIC_ENABLE, "TRIOPTIC_ENABLE");

        put(PropertyID.UPCA_ENABLE, "UPCA_ENABLE");
        put(PropertyID.UPCA_SEND_CHECK, "UPCA_SEND_CHECK");
        put(PropertyID.UPCA_SEND_SYS, "UPCA_SEND_SYS");
        put(PropertyID.UPCA_TO_EAN13, "UPCA_TO_EAN13");

        put(PropertyID.UPCE_ENABLE, "UPCE_ENABLE");
        put(PropertyID.UPCE_SEND_CHECK, "UPCE_SEND_CHECK");
        put(PropertyID.UPCE_SEND_SYS, "UPCE_SEND_SYS");
        put(PropertyID.UPCE_TO_UPCA, "UPCE_TO_UPCA");

        put(PropertyID.UPCE1_ENABLE, "UPCE1_ENABLE");
    }};

}