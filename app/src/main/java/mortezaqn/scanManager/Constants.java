package mortezaqn.scanManager;

import static android.device.scanner.configuration.Constants.Symbology.AZTEC;
import static android.device.scanner.configuration.Constants.Symbology.CHINESE25;
import static android.device.scanner.configuration.Constants.Symbology.CODABAR;
import static android.device.scanner.configuration.Constants.Symbology.CODE11;
import static android.device.scanner.configuration.Constants.Symbology.CODE128;
import static android.device.scanner.configuration.Constants.Symbology.CODE32;
import static android.device.scanner.configuration.Constants.Symbology.CODE39;
import static android.device.scanner.configuration.Constants.Symbology.CODE93;
import static android.device.scanner.configuration.Constants.Symbology.COMPOSITE_CC_AB;
import static android.device.scanner.configuration.Constants.Symbology.COMPOSITE_CC_C;
import static android.device.scanner.configuration.Constants.Symbology.COMPOSITE_TLC39;
import static android.device.scanner.configuration.Constants.Symbology.DATAMATRIX;
import static android.device.scanner.configuration.Constants.Symbology.DISCRETE25;
import static android.device.scanner.configuration.Constants.Symbology.EAN13;
import static android.device.scanner.configuration.Constants.Symbology.EAN8;
import static android.device.scanner.configuration.Constants.Symbology.GS1_128;
import static android.device.scanner.configuration.Constants.Symbology.GS1_14;
import static android.device.scanner.configuration.Constants.Symbology.GS1_EXP;
import static android.device.scanner.configuration.Constants.Symbology.GS1_LIMIT;
import static android.device.scanner.configuration.Constants.Symbology.HANXIN;
import static android.device.scanner.configuration.Constants.Symbology.INTERLEAVED25;
import static android.device.scanner.configuration.Constants.Symbology.MATRIX25;
import static android.device.scanner.configuration.Constants.Symbology.MAXICODE;
import static android.device.scanner.configuration.Constants.Symbology.MICROPDF417;
import static android.device.scanner.configuration.Constants.Symbology.MSI;
import static android.device.scanner.configuration.Constants.Symbology.NONE;
import static android.device.scanner.configuration.Constants.Symbology.PDF417;
import static android.device.scanner.configuration.Constants.Symbology.POSTAL_4STATE;
import static android.device.scanner.configuration.Constants.Symbology.POSTAL_AUSTRALIAN;
import static android.device.scanner.configuration.Constants.Symbology.POSTAL_JAPAN;
import static android.device.scanner.configuration.Constants.Symbology.POSTAL_KIX;
import static android.device.scanner.configuration.Constants.Symbology.POSTAL_PLANET;
import static android.device.scanner.configuration.Constants.Symbology.POSTAL_POSTNET;
import static android.device.scanner.configuration.Constants.Symbology.POSTAL_ROYALMAIL;
import static android.device.scanner.configuration.Constants.Symbology.POSTAL_UPUFICS;
import static android.device.scanner.configuration.Constants.Symbology.QRCODE;
import static android.device.scanner.configuration.Constants.Symbology.RESERVED_13;
import static android.device.scanner.configuration.Constants.Symbology.RESERVED_15;
import static android.device.scanner.configuration.Constants.Symbology.RESERVED_16;
import static android.device.scanner.configuration.Constants.Symbology.RESERVED_20;
import static android.device.scanner.configuration.Constants.Symbology.RESERVED_21;
import static android.device.scanner.configuration.Constants.Symbology.RESERVED_27;
import static android.device.scanner.configuration.Constants.Symbology.RESERVED_28;
import static android.device.scanner.configuration.Constants.Symbology.RESERVED_30;
import static android.device.scanner.configuration.Constants.Symbology.RESERVED_33;
import static android.device.scanner.configuration.Constants.Symbology.RESERVED_6;
import static android.device.scanner.configuration.Constants.Symbology.TRIOPTIC;
import static android.device.scanner.configuration.Constants.Symbology.UPCA;
import static android.device.scanner.configuration.Constants.Symbology.UPCE;
import static android.device.scanner.configuration.Constants.Symbology.UPCE1;

import android.device.scanner.configuration.Symbology;

class Constants {

    static final String ACTION_CAPTURE_IMAGE = "scanner_capture_image_result";
    static final String DECODE_ENABLE = "decode_enable";
    static final String DECODE_TRIGGER_MODE = "decode_trigger_mode";
    static final String DECODE_TRIGGER_MODE_HOST = "HOST";
    static final String DECODE_TRIGGER_MODE_CONTINUOUS = "CONTINUOUS";
    static final String DECODE_TRIGGER_MODE_PAUSE = "PAUSE";
    static final String DECODE_OUTPUT_MODE = "decode_output_mode";

    static final String DECODE_CAPTURE_IMAGE_SHOW = "scan_capture_image";
    static final int MSG_SHOW_SCAN_RESULT = 1;
    static final int MSG_SHOW_SCAN_IMAGE = 2;
    static final int[] SCAN_KEYCODE = {520, 521, 522, 523};

    static final String SCAN_SERVICE_ONOFF = "scan_turnon_switch";
    static final String SCAN_TRIGGER_LOCK = "scan_trigger_lock";
    static final String SCAN_TRIGGER_MODE = "scan_trigger_mode";
    static final String SCAN_RESET = "scan_reset_def";
    static final String SCAN_OUTPUT_MODE = "scan_output_mode";
    static final String SCAN_KEYBOARD_OUTPUT_TYPE = "scan_keyboard_output_type";
    static final String SCAN_SOUND_MODE = "scan_sound_mode";
    static final String SCAN_INTENT_ACTION = "scan_intent_action";
    static final String SCAN_INTENT_LABEL = "scan_intent_stringlabel";
    static final String SCAN_INTENT_BARCODE_TYPE = "scan_intent_barcode_type";
    static final String SCAN_CAPTURE_IMAGE = "scan_cupture_image";
    static final String SCAN_BARCODE_SYMBOLOGY_LIST_KEY = "scan_barcode_symbology_list";
    static final String SCAN_PULSE = "0";
    static final String SCAN_CONTINUOUS = "1";
    static final String SCAN_HOST = "2";

    /**
     * Use of android.device.scanner.configuration.Symbology enums
     */
    static final Symbology[] BARCODE_SUPPORT_SYMBOLOGY = new Symbology[]{
            Symbology.AZTEC,
            Symbology.CHINESE25,
            Symbology.CODABAR,
            Symbology.CODE11,
            Symbology.CODE32,
            Symbology.CODE39,
            Symbology.CODE93,
            Symbology.CODE128,
            Symbology.COMPOSITE_CC_AB,
            Symbology.COMPOSITE_CC_C,
            Symbology.DATAMATRIX,
            Symbology.DISCRETE25,
            Symbology.EAN8,
            Symbology.EAN13,
            Symbology.GS1_14,
            Symbology.GS1_128,
            Symbology.GS1_EXP,
            Symbology.GS1_LIMIT,
            Symbology.INTERLEAVED25,
            Symbology.MATRIX25,
            Symbology.MAXICODE,
            Symbology.MICROPDF417,
            Symbology.MSI,
            Symbology.PDF417,
            Symbology.POSTAL_4STATE,
            Symbology.POSTAL_AUSTRALIAN,
            Symbology.POSTAL_JAPAN,
            Symbology.POSTAL_KIX,
            Symbology.POSTAL_PLANET,
            Symbology.POSTAL_POSTNET,
            Symbology.POSTAL_ROYALMAIL,
            Symbology.POSTAL_UPUFICS,
            Symbology.QRCODE,
            Symbology.TRIOPTIC,
            Symbology.UPCA,
            Symbology.UPCE,
            Symbology.UPCE1,
            Symbology.NONE
    };

    /**
     * Use of android.device.scanner.configuration.Constants.Symbology Class
     */
    int[] BARCODE_SYMBOLOGY = new int[]{
            AZTEC,
            CHINESE25,
            CODABAR,
            CODE11,
            CODE32,
            CODE39,
            CODE93,
            CODE128,
            COMPOSITE_CC_AB,
            COMPOSITE_CC_C,
            COMPOSITE_TLC39,
            DATAMATRIX,
            DISCRETE25,
            EAN8,
            EAN13,
            GS1_14,
            GS1_128,
            GS1_EXP,
            GS1_LIMIT,
            HANXIN,
            INTERLEAVED25,
            MATRIX25,
            MAXICODE,
            MICROPDF417,
            MSI,
            PDF417,
            POSTAL_4STATE,
            POSTAL_AUSTRALIAN,
            POSTAL_JAPAN,
            POSTAL_KIX,
            POSTAL_PLANET,
            POSTAL_POSTNET,
            POSTAL_ROYALMAIL,
            POSTAL_UPUFICS,
            QRCODE,
            TRIOPTIC,
            UPCA,
            UPCE,
            UPCE1,
            NONE,
            RESERVED_6,
            RESERVED_13,
            RESERVED_15,
            RESERVED_16,
            RESERVED_20,
            RESERVED_21,
            RESERVED_27,
            RESERVED_28,
            RESERVED_30,
            RESERVED_33
    };
}