package mortezaqn.scanManager;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.mortezaqn.samplescannerurovo.R;

/**
 * Perform reset operation class
 */
@SuppressLint("StaticFieldLeak")
class ResetAsyncTask extends AsyncTask<String, String, Integer> {
    private final ScanManagerDemo mScanDemo;
    private final ScanSettingsFragment mScanSettingsFragment;
    private ProgressDialog progressDialog;

    public ResetAsyncTask(ScanManagerDemo mScanDemo, ScanSettingsFragment mScanSettingsFragment) {
        this.mScanDemo = mScanDemo;
        this.mScanSettingsFragment = mScanSettingsFragment;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        progressDialog = new ProgressDialog(mScanDemo);
        progressDialog.setMessage(mScanDemo.getResources().getString(R.string.scan_reset_progress));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    /**
     * ScanManager.resetScannerParameters
     *
     * @param params
     * @return
     */
    @Override
    protected Integer doInBackground(String... params) {
        // TODO Auto-generated method stub
        try {
            mScanDemo.mScanManager.resetScannerParameters();
            mScanDemo.setScanOutputMode(mScanDemo.DECODE_OUTPUT_MODE_FOCUS);
            Thread.sleep(500);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    protected void onPostExecute(Integer result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if (progressDialog != null)
            progressDialog.dismiss();
        mScanSettingsFragment.resetScan();
        Toast.makeText(mScanDemo, R.string.scanner_toast, Toast.LENGTH_LONG).show();
    }

}