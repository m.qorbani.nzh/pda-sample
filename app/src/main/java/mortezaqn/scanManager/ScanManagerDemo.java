package mortezaqn.scanManager;

import static mortezaqn.scanManager.Constants.ACTION_CAPTURE_IMAGE;
import static mortezaqn.scanManager.Constants.DECODE_CAPTURE_IMAGE_SHOW;
import static mortezaqn.scanManager.Constants.DECODE_ENABLE;
import static mortezaqn.scanManager.Constants.DECODE_OUTPUT_MODE;
import static mortezaqn.scanManager.Constants.DECODE_TRIGGER_MODE;
import static mortezaqn.scanManager.Constants.DECODE_TRIGGER_MODE_CONTINUOUS;
import static mortezaqn.scanManager.Constants.DECODE_TRIGGER_MODE_HOST;
import static mortezaqn.scanManager.Constants.DECODE_TRIGGER_MODE_PAUSE;
import static mortezaqn.scanManager.Constants.MSG_SHOW_SCAN_IMAGE;
import static mortezaqn.scanManager.Constants.MSG_SHOW_SCAN_RESULT;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.device.ScanManager;
import android.device.scanner.configuration.PropertyID;
import android.device.scanner.configuration.Symbology;
import android.device.scanner.configuration.Triggering;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.mortezaqn.samplescannerurovo.R;

import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

/**
 * ScanManagerDemo
 *
 * @author shenpidong
 * @effect Introduce the use of android.device.ScanManager
 * @date 2020-03-06
 * @description , Steps to use ScanManager:
 * <p>
 * 1.Obtain an instance of BarCodeReader with ScanManager scan = new ScanManager().
 * 2.Call openScanner to power on the barcode reader.
 * 3.The default output mode is TextBox Mode that send barcode data to the focused text box.
 * User can check the output mode using "getOutputMode" and set the output mode using "switchOutputMode".
 * 4.The default trigger mode is manually trigger signal.
 * User can check the trigger mode using "getTriggerMode" and set the trigger mode using "setTriggerMode".
 * 5.If necessary, check
 * The current settings using "getParameterInts" or
 * set the scanner configuration properties PropertyID using "setParameterInts".
 * 6.To begin a decode session, call startDecode.
 * If the configured PropertyID.WEDGE_KEYBOARD_ENABLE is 0,
 * your registered broadcast receiver will be called when a successful decode occurs.
 * 7.If the output mode is intent mode, the captured data is sent as an implicit Intent.
 * An application interests in the scan data
 * should register an action as "android.intent.ACTION_DECODE_DATA" broadcast listener.
 * 8.To get a still image through an Android intent.
 * Register the "scanner_capture_image_result" broadcast reception image,
 * trigger the scan to listen to the result output and send the "action.scanner_capture_image" broadcast request
 * to the scan service to output the image.
 * 9.Call stopDecode to end the decode session.
 * 10.Call closeScanner to power off the barcode reader.
 * 11.Can set parameters before closing the scan service.
 */
public class ScanManagerDemo extends AppCompatActivity {
    private static final Map<String, BarcodeHolder> mBarcodeMap = new HashMap<>();
    final int DECODE_OUTPUT_MODE_INTENT = 0;
    final int DECODE_OUTPUT_MODE_FOCUS = 1;
    private final ScanSettingsFragment mScanSettingsFragment = new ScanSettingsFragment();
    //    private int DECODE_OUTPUT_MODE_CURRENT = DECODE_OUTPUT_MODE_FOCUS;
    ScanManager mScanManager = null;
    // region view
    EditText showScanResult = null;
    Button mScan = null;
    LinearLayout mHome = null;
    FrameLayout mFlagment = null;
    MenuItem settings = null;
    ImageView mScanImage = null;
    //    private String DECODE_TRIGGER_MODE_CURRENT = DECODE_TRIGGER_MODE_HOST;
    private boolean mScanEnable = true;
    private boolean mScanCaptureImageShow = false;
    // endregion view
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @SuppressLint("HandlerLeak")
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_SHOW_SCAN_RESULT:
                    String scanResult = (String) msg.obj;
                    printScanResult(scanResult);
                    break;
                case MSG_SHOW_SCAN_IMAGE:
                    if (mScanImage != null && mScanCaptureImageShow) {
                        Bitmap bitmap = (Bitmap) msg.obj;
                        mScanImage.setImageBitmap(bitmap);
                        mScanImage.setVisibility(View.VISIBLE);
                    } else {
                        mScanCaptureImageShow = false;
                        assert mScanImage != null;
                        mScanImage.setVisibility(View.INVISIBLE);
                        Timber.i("handleMessage , MSG_SHOW_SCAN_IMAGE scan image:%s", mScanImage);
                    }
                    break;
            }
        }
    };
    private final BroadcastReceiver mReceiver = new ScanReceiver(mHandler);
    private FrameLayout mScanSettingsMenuBarcodeList = null;
    private FrameLayout mScanSettingsMenuBarcode = null;
    private boolean mScanSettingsView = false;
    private boolean mScanBarcodeSettingsMenuBarcodeList = false;
    private boolean mScanBarcodeSettingsMenuBarcode = false;
    private SharedPreferences sharedPrefs;

    public boolean isScanEnable() {
        return mScanEnable;
    }

    public void setScanEnable(boolean mScanEnable) {this.mScanEnable = mScanEnable;}

    public void setScanCaptureImageShow(boolean mScanCaptureImageShow) {this.mScanCaptureImageShow = mScanCaptureImageShow;}

    private int getDecodeIntShared(String key) {return sharedPrefs.getInt(key, 1);}

    /**
     * Attribute helper
     */
    private void updateIntShared(String key, int value) {
        if (key == null || "".equals(key.trim())) {
            Timber.i("updateIntShared , key:%s", key);
            return;
        }
        if (value == getDecodeIntShared(key)) {
            Timber.i("updateIntShared ,ignore key:" + key + " update.");
            return;
        }
        Editor editor = sharedPrefs.edit();
        editor.putInt(key, value);
        editor.apply();
        editor.commit();
    }

    private String getDecodeStringShared(String key) {
        return sharedPrefs.getString(key, "");
    }

    /**
     * Attribute helper
     */
    private void updateStringShared(String key, String value) {
        if (key == null || "".equals(key.trim())) {
            Timber.i("updateStringShared , key:%s", key);
            return;
        }
        //        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (value.equals(getDecodeStringShared(key)) || "".equals(value.trim())) {
            Timber.i("updateStringShared ,ignore key:" + key + " update.");
            return;
        }
        Editor editor = sharedPrefs.edit();
        editor.putString(key, value);
        editor.apply();
        editor.commit();
    }

    boolean getDecodeScanShared(String key) {
        return sharedPrefs.getBoolean(key, true);
    }

    /**
     * ScanManager.setTriggerMode
     *
     * @param mode value : Triggering.HOST, Triggering.CONTINUOUS, or Triggering.PULSE.
     */
    void setTrigger(Triggering mode) {
        Triggering currentMode = mScanManager.getTriggerMode();
        Timber.d("setTrigger , mode;" + mode + ",currentMode:" + currentMode);
        if (mode != currentMode) {
            mScanManager.setTriggerMode(mode);
            switch (mode) {
                case HOST:
                    //                    DECODE_TRIGGER_MODE_CURRENT = DECODE_TRIGGER_MODE_HOST;
                    updateStringShared(DECODE_TRIGGER_MODE, DECODE_TRIGGER_MODE_HOST);
                    break;
                case CONTINUOUS:
                    //                    DECODE_TRIGGER_MODE_CURRENT = DECODE_TRIGGER_MODE_CONTINUOUS;
                    updateStringShared(DECODE_TRIGGER_MODE, DECODE_TRIGGER_MODE_CONTINUOUS);
                    break;
                case PULSE:
                    //                    DECODE_TRIGGER_MODE_CURRENT = DECODE_TRIGGER_MODE_PAUSE;
                    updateStringShared(DECODE_TRIGGER_MODE, DECODE_TRIGGER_MODE_PAUSE);
                    break;
            }
        } else {
            Timber.i("setTrigger , ignore update Trigger mode:%s", mode);
        }
    }

    /**
     * ScanManager.switchOutputMode
     *
     * @param mode
     */
    void setScanOutputMode(int mode) {
        int currentMode = mScanManager.getOutputMode();
        if (mode != currentMode && (mode == DECODE_OUTPUT_MODE_FOCUS || mode == DECODE_OUTPUT_MODE_INTENT)) {
            mScanManager.switchOutputMode(mode);
            if (mode == DECODE_OUTPUT_MODE_FOCUS) {
                //                DECODE_OUTPUT_MODE_CURRENT = DECODE_OUTPUT_MODE_FOCUS;
                updateIntShared(DECODE_OUTPUT_MODE, DECODE_OUTPUT_MODE_FOCUS);
            } else /*if (mode == DECODE_OUTPUT_MODE_INTENT)*/ {
                //                DECODE_OUTPUT_MODE_CURRENT = DECODE_OUTPUT_MODE_INTENT;
                updateIntShared(DECODE_OUTPUT_MODE, DECODE_OUTPUT_MODE_INTENT);
            }
        } else {
            Timber.i("setScanOutputMode , ignore update Output mode:%s", mode);
        }
    }

    /**
     * @param register , ture register , false unregister
     */
    @SuppressLint("UnspecifiedRegisterReceiverFlag")
    void registerReceiver(boolean register) {
        if (register && mScanManager != null) {
            IntentFilter filter = new IntentFilter();
            int[] idBuffer = new int[]{PropertyID.WEDGE_INTENT_ACTION_NAME, PropertyID.WEDGE_INTENT_DATA_STRING_TAG};
            String[] value_buf = mScanManager.getParameterString(idBuffer);
            if (value_buf != null && value_buf[0] != null && !value_buf[0].equals("")) {
                filter.addAction(value_buf[0]);
            } else {
                filter.addAction(ScanManager.ACTION_DECODE);
            }
            filter.addAction(ACTION_CAPTURE_IMAGE);

            registerReceiver(mReceiver, filter);
        } else if (mScanManager != null) {
            mScanManager.stopDecode();
            unregisterReceiver(mReceiver);
        }
    }

    /**
     * Intent Output Mode print scan results.
     *
     * @param msg
     */
    private void printScanResult(String msg) {
        if (msg == null || showScanResult == null) {
            Timber.i("printScanResult , ignore to show msg:" + msg + ",showScanResult" + showScanResult);
            return;
        }
        showScanResult.setText(msg);
    }

    private void initView() {
        mScanEnable = getDecodeScanShared(DECODE_ENABLE);

        showScanResult = findViewById(R.id.scan_result);
        mScan = findViewById(R.id.scan_trigger);

        ButtonListener listener = new ButtonListener(this);
        mScan.setOnTouchListener(listener);
        mScan.setOnClickListener(listener);
        mScanImage = findViewById(R.id.scan_image);

        mScanCaptureImageShow = getDecodeScanShared(DECODE_CAPTURE_IMAGE_SHOW);

        updateCaptureImage();
        mFlagment = findViewById(R.id.fl);
        mScanSettingsMenuBarcodeList = findViewById(R.id.flagment_menu_barcode_list);
        mScanSettingsMenuBarcode = findViewById(R.id.flagment_menu_barcode);
        mHome = findViewById(R.id.homeshow);
    }

    void updateCaptureImage() {
        if (mScanImage == null) {
            Timber.i("updateCaptureImage ignore.");
            return;
        }
        if (mScanCaptureImageShow) {
            mScanImage.setVisibility(View.VISIBLE);
        } else {
            mScanImage.setVisibility(View.INVISIBLE);
        }
    }

    private void scanSettingsUpdate() {
        Timber.d("scanSettingsUpdate");
        FragmentManager fm = this.getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        mScanSettingsFragment.setScanManagerDemo(this);
        ft.replace(R.id.fl, mScanSettingsFragment);
        ft.commit();
        mScanSettingsView = true;
        mHome.setVisibility(View.GONE);

        mScanBarcodeSettingsMenuBarcodeList = false;
        mScanBarcodeSettingsMenuBarcode = false;
        mScanSettingsMenuBarcode.setVisibility(View.GONE);
        mScanSettingsMenuBarcodeList.setVisibility(View.GONE);

        if (settings != null)
            settings.setVisible(false);

        mFlagment.setVisibility(View.VISIBLE);
    }

    /**
     * helper
     */
    void scanSettingsBarcodeList() {
        Timber.d("scanSettingsBarcodeList");

        FragmentManager fragmentManager = this.getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        SettingsBarcodeList mSettingsBarcodeList = new SettingsBarcodeList();
        mSettingsBarcodeList.setScanManagerDemo(this);
        fragmentTransaction.replace(R.id.flagment_menu_barcode_list, mSettingsBarcodeList);
        fragmentTransaction.commit();

        mScanSettingsView = true;
        mScanBarcodeSettingsMenuBarcodeList = true;
        mScanBarcodeSettingsMenuBarcode = false;
        mFlagment.setVisibility(View.GONE);
        mHome.setVisibility(View.GONE);
        mScanSettingsMenuBarcode.setVisibility(View.GONE);
        mScanSettingsMenuBarcodeList.setVisibility(View.VISIBLE);
        if (settings != null) {
            settings.setVisible(false);
        }
    }

    void updateScanSettingsBarcode(String key) {
        mScanSettingsView = true;
        mScanBarcodeSettingsMenuBarcodeList = true;
        mScanBarcodeSettingsMenuBarcode = true;
        Timber.d("updateScanSettingsBarcode , key:%s", key);
        FragmentManager fm = this.getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Timber.d("updateScanSettingsBarcode , isEmpty:");
        ScanSettingsBarcode mScanSettingsBarcode = new ScanSettingsBarcode();
        mScanSettingsBarcode.setBarcodeMap(mBarcodeMap);
        mScanSettingsBarcode.setScanManagerDemo(this, key);
        ft.replace(R.id.flagment_menu_barcode, mScanSettingsBarcode);
        ft.commit();
        mHome.setVisibility(View.GONE);
        mFlagment.setVisibility(View.GONE);
        mScanSettingsMenuBarcodeList.setVisibility(View.GONE);
        mScanSettingsMenuBarcode.setVisibility(View.VISIBLE);
        if (settings != null) {
            settings.setVisible(false);
        }
    }

    /**
     * Attribute helper
     *
     * @param key
     * @param enable
     */
    void updateScanShared(String key, boolean enable) {
        if (key == null || "".equals(key.trim())) {
            Timber.i("updateScanShared , key:%s", key);
            return;
        }
        if (enable == getDecodeScanShared(key)) {
            Timber.i("updateScanShared ,ignore key:" + key + " update.");
            return;
        }
        Editor editor = sharedPrefs.edit();
        editor.putBoolean(key, enable);
        editor.apply();
        editor.commit();
    }

    private void initScan() {
        // Step-01
        mScanManager = new ScanManager();
        boolean powerOn = mScanManager.getScannerState();
        if (!powerOn) {
            // Step-02
            powerOn = mScanManager.openScanner();
            if (!powerOn) {
                new AlertDialog.Builder(this).setMessage("Scanner cannot be turned on!")
                                             .setPositiveButton("OK", (dialog, which) -> dialog.dismiss())
                                             .create()
                                             .show();
            }
        }
        initBarcodeParameters();
    }

    void resetScanner() {
        showResetDialog();
    }

    /**
     * ScanManager.lockTrigger and ScanManager.unlockTrigger
     *
     * @param state value ture or false
     */
    void updateLockTriggerState(boolean state) {
        boolean currentState = mScanManager.getTriggerLockState();
        if (state != currentState) {
            if (state) {
                mScanManager.lockTrigger();
            } else {
                mScanManager.unlockTrigger();
            }
        } else {
            Timber.i("updateLockTriggerState , ignore update lockTrigger state:%s", state);
        }
    }

    /**
     * ScanManager.closeScanner
     *
     * @return
     */
    boolean closeScanner() {
        boolean state = false;
        if (mScanManager != null) {
            mScanManager.stopDecode();
            state = mScanManager.closeScanner();
        }
        return state;
    }

    /**
     * Obtain an instance of BarCodeReader with ScanManager
     * ScanManager.getScannerState
     * ScanManager.openScanner
     * ScanManager.enableAllSymbologies
     */
    boolean openScanner() {
        mScanManager = new ScanManager();
        boolean powerOn = mScanManager.getScannerState();
        if (!powerOn) {
            powerOn = mScanManager.openScanner();
            if (!powerOn) {
                new AlertDialog.Builder(this).setMessage("Scanner cannot be turned on!")
                                             .setPositiveButton("OK", (dialog, which) -> dialog.dismiss())
                                             .create()
                                             .show();
            }
        }
        mScanManager.enableAllSymbologies(true);
        // or execute enableSymbologyDemo() || enableSymbologyDemo2() is the same.
        setTrigger(mScanManager.getTriggerMode());
        setScanOutputMode(mScanManager.getOutputMode());
        return powerOn;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_scan_manager_demo);
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Step-01
        initScan();
        registerReceiver(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        registerReceiver(false);
    }

    @Override
    public void onBackPressed() {
        if (mScanBarcodeSettingsMenuBarcode) {
            mScanBarcodeSettingsMenuBarcode = false;
            mHome.setVisibility(View.GONE);
            mFlagment.setVisibility(View.GONE);
            mScanSettingsMenuBarcode.setVisibility(View.GONE);
            mScanSettingsMenuBarcodeList.setVisibility(View.VISIBLE);
            if (settings != null) {
                settings.setVisible(false);
            }
        } else if (mScanBarcodeSettingsMenuBarcodeList) {
            mScanBarcodeSettingsMenuBarcodeList = false;
            mHome.setVisibility(View.GONE);
            mFlagment.setVisibility(View.VISIBLE);
            mScanSettingsMenuBarcode.setVisibility(View.GONE);
            mScanSettingsMenuBarcodeList.setVisibility(View.GONE);
            if (settings != null) {
                settings.setVisible(false);
            }
        } else if (mScanSettingsView) {
            mHome.setVisibility(View.VISIBLE);
            mFlagment.setVisibility(View.GONE);
            if (settings != null) {
                settings.setVisible(true);
            }
            mScanSettingsView = false;
        } else {
            super.onBackPressed();
        }
        Timber.i("onBackPressed");
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Timber.d("onKeyUp, keyCode:%s", keyCode);
        //        if (keyCode >= SCAN_KEYCODE[0] && keyCode <= SCAN_KEYCODE[SCAN_KEYCODE.length - 1]) {}
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Timber.d("onKeyDown, keyCode:%s", keyCode);
        //        if (keyCode >= SCAN_KEYCODE[0] && keyCode <= SCAN_KEYCODE[SCAN_KEYCODE.length - 1]) {}
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Top right corner setting button
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //        settings = menu.add(0, 1, 0, R.string.scan_settings).setIcon(R.drawable.ic_action_settings);
        settings.setShowAsAction(1);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Set the button monitor in the upper right corner
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Timber.d("onOptionsItemSelected, item:%s", item.getItemId());
        if (item.getItemId() == 1) {
            scanSettingsUpdate();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Reset Auxiliary dialog
     */
    private void showResetDialog() {
        new AlertDialog.Builder(this).setMessage(R.string.scan_reset_def_alert)
                                     .setTitle(R.string.scan_reset_def)
                                     .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                                         ResetAsyncTask task = new ResetAsyncTask(this, new ScanSettingsFragment());
                                         task.execute("reset");
                                         dialog.dismiss();
                                     })
                                     .setNegativeButton(android.R.string.no, (dialog, which) -> dialog.dismiss())
                                     .create()
                                     .show();
    }

    /**
     * mBarcodeMap helper
     */
    private void initBarcodeParameters() {
        mBarcodeMap.clear();
        // region Symbology.AZTEC
        BarcodeHolder holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.AZTEC_ENABLE};
        holder.mParaKeys = new String[]{"AZTEC_ENABLE"};
        mBarcodeMap.put(Symbology.AZTEC + "", holder);
        // endregion Symbology.AZTEC
        // region Symbology.CHINESE25
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.C25_ENABLE};
        holder.mParaKeys = new String[]{"C25_ENABLE"};
        mBarcodeMap.put(Symbology.CHINESE25 + "", holder);
        // endregion Symbology.CHINESE25
        // region Symbology.CODABAR
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mBarcodeLength1 = new EditTextPreference(this);
        holder.mBarcodeLength2 = new EditTextPreference(this);
        holder.mBarcodeNOTIS = new CheckBoxPreference(this);
        holder.mBarcodeCLSI = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.CODABAR_ENABLE, PropertyID.CODABAR_LENGTH1, PropertyID.CODABAR_LENGTH2, PropertyID.CODABAR_NOTIS, PropertyID.CODABAR_CLSI};
        holder.mParaKeys = new String[]{"CODABAR_ENABLE", "CODABAR_LENGTH1", "CODABAR_LENGTH2", "CODABAR_NOTIS", "CODABAR_CLSI"};
        mBarcodeMap.put(Symbology.CODABAR + "", holder);
        // endregion Symbology.CODABAR
        // region Symbology.CODE11
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mBarcodeLength1 = new EditTextPreference(this);
        holder.mBarcodeLength2 = new EditTextPreference(this);
        holder.mBarcodeCheckDigit = new ListPreference(this);
        holder.mParaIds = new int[]{PropertyID.CODE11_ENABLE, PropertyID.CODE11_LENGTH1, PropertyID.CODE11_LENGTH2, PropertyID.CODE11_SEND_CHECK};
        holder.mParaKeys = new String[]{"CODE11_ENABLE", "CODE11_LENGTH1", "CODE11_LENGTH2", "CODE11_SEND_CHECK"};
        mBarcodeMap.put(Symbology.CODE11 + "", holder);
        // endregion Symbology.CODE11
        // region Symbology.CODE32
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.CODE32_ENABLE};
        holder.mParaKeys = new String[]{"CODE32_ENABLE"};
        mBarcodeMap.put(Symbology.CODE32 + "", holder);
        // endregion Symbology.CODE32
        // region Symbology.CODE39
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mBarcodeLength1 = new EditTextPreference(this);
        holder.mBarcodeLength2 = new EditTextPreference(this);
        holder.mBarcodeChecksum = new CheckBoxPreference(this);
        holder.mBarcodeSendCheck = new CheckBoxPreference(this);
        holder.mBarcodeFullASCII = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.CODE39_ENABLE, PropertyID.CODE39_LENGTH1, PropertyID.CODE39_LENGTH2, PropertyID.CODE39_ENABLE_CHECK, PropertyID.CODE39_SEND_CHECK, PropertyID.CODE39_FULL_ASCII};
        holder.mParaKeys = new String[]{"CODE39_ENABLE", "CODE39_LENGTH1", "CODE39_LENGTH2", "CODE39_ENABLE_CHECK", "CODE39_SEND_CHECK", "CODE39_FULL_ASCII"};
        mBarcodeMap.put(Symbology.CODE39 + "", holder);
        // endregion Symbology.CODE39
        // region Symbology.CODE93
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mBarcodeLength1 = new EditTextPreference(this);
        holder.mBarcodeLength2 = new EditTextPreference(this);
        holder.mParaIds = new int[]{PropertyID.CODE93_ENABLE, PropertyID.CODE93_LENGTH1, PropertyID.CODE93_LENGTH2};
        holder.mParaKeys = new String[]{"CODE93_ENABLE", "CODE93_LENGTH1", "CODE93_LENGTH2"};
        mBarcodeMap.put(Symbology.CODE93 + "", holder);
        // endregion Symbology.CODE93
        // region Symbology.CODE128
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mBarcodeLength1 = new EditTextPreference(this);
        holder.mBarcodeLength2 = new EditTextPreference(this);
        holder.mBarcodeISBT = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.CODE128_ENABLE, PropertyID.CODE128_LENGTH1, PropertyID.CODE128_LENGTH2, PropertyID.CODE128_CHECK_ISBT_TABLE};
        holder.mParaKeys = new String[]{"CODE128_ENABLE", "CODE128_LENGTH1", "CODE128_LENGTH2", "CODE128_CHECK_ISBT_TABLE"};
        mBarcodeMap.put(Symbology.CODE128 + "", holder);
        // endregion Symbology.CODE128
        // region Symbology.COMPOSITE_CC_AB
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.COMPOSITE_CC_AB_ENABLE};
        holder.mParaKeys = new String[]{"COMPOSITE_CC_AB_ENABLE"};
        mBarcodeMap.put(Symbology.COMPOSITE_CC_AB + "", holder);
        // endregion Symbology.COMPOSITE_CC_AB
        // region Symbology.COMPOSITE_CC_C
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.COMPOSITE_CC_C_ENABLE};
        holder.mParaKeys = new String[]{"COMPOSITE_CC_C_ENABLE"};
        mBarcodeMap.put(Symbology.COMPOSITE_CC_C + "", holder);
        // endregion Symbology.COMPOSITE_CC_C
        // region Symbology.DATAMATRIX
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.DATAMATRIX_ENABLE};
        holder.mParaKeys = new String[]{"DATAMATRIX_ENABLE"};
        mBarcodeMap.put(Symbology.DATAMATRIX + "", holder);
        // endregion Symbology.DATAMATRIX
        // region Symbology.DISCRETE25
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.D25_ENABLE};
        holder.mParaKeys = new String[]{"D25_ENABLE"};
        mBarcodeMap.put(Symbology.DISCRETE25 + "", holder);
        // endregion Symbology.DISCRETE25
        // region Symbology.EAN8
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.EAN8_ENABLE};
        holder.mParaKeys = new String[]{"EAN8_ENABLE"};
        mBarcodeMap.put(Symbology.EAN8 + "", holder);
        // endregion Symbology.EAN8
        // region Symbology.EAN13
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mBarcodeBookland = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.EAN13_ENABLE, PropertyID.EAN13_BOOKLANDEAN};
        holder.mParaKeys = new String[]{"EAN13_ENABLE", "EAN13_BOOKLANDEAN"};
        mBarcodeMap.put(Symbology.EAN13 + "", holder);
        // endregion Symbology.EAN13
        // region Symbology.GS1_14
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.GS1_14_ENABLE};
        holder.mParaKeys = new String[]{"GS1_14_ENABLE"};
        mBarcodeMap.put(Symbology.GS1_14 + "", holder);
        // endregion Symbology.GS1_14
        // region Symbology.GS1_128
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.CODE128_GS1_ENABLE};
        holder.mParaKeys = new String[]{"CODE128_GS1_ENABLE"};
        mBarcodeMap.put(Symbology.GS1_128 + "", holder);
        // endregion Symbology.GS1_128
        // region Symbology.GS1_EXP
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mBarcodeLength1 = new EditTextPreference(this);
        holder.mBarcodeLength2 = new EditTextPreference(this);
        holder.mParaIds = new int[]{PropertyID.GS1_EXP_ENABLE, PropertyID.GS1_EXP_LENGTH1, PropertyID.GS1_EXP_LENGTH2};
        holder.mParaKeys = new String[]{"GS1_EXP_ENABLE", "GS1_EXP_LENGTH1", "GS1_EXP_LENGTH2"};
        mBarcodeMap.put(Symbology.GS1_EXP + "", holder);
        // endregion Symbology.GS1_EXP
        // region Symbology.GS1_LIMIT
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.GS1_LIMIT_ENABLE};
        holder.mParaKeys = new String[]{"GS1_LIMIT_ENABLE"};
        mBarcodeMap.put(Symbology.GS1_LIMIT + "", holder);
        // endregion Symbology.GS1_LIMIT
        // region Symbology.INTERLEAVED25
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mBarcodeLength1 = new EditTextPreference(this);
        holder.mBarcodeLength2 = new EditTextPreference(this);
        holder.mBarcodeChecksum = new CheckBoxPreference(this);
        holder.mBarcodeSendCheck = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.I25_ENABLE, PropertyID.I25_LENGTH1, PropertyID.I25_LENGTH2, PropertyID.I25_ENABLE_CHECK, PropertyID.I25_SEND_CHECK};
        holder.mParaKeys = new String[]{"I25_ENABLE", "I25_LENGTH1", "I25_LENGTH2", "I25_ENABLE_CHECK", "I25_SEND_CHECK"};
        mBarcodeMap.put(Symbology.INTERLEAVED25 + "", holder);
        // endregion Symbology.INTERLEAVED25
        // region Symbology.MATRIX25
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.M25_ENABLE};
        holder.mParaKeys = new String[]{"M25_ENABLE"};
        mBarcodeMap.put(Symbology.MATRIX25 + "", holder);
        // endregion Symbology.MATRIX25
        // region Symbology.MAXICODE
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.MAXICODE_ENABLE};
        holder.mParaKeys = new String[]{"MAXICODE_ENABLE"};
        mBarcodeMap.put(Symbology.MAXICODE + "", holder);
        // endregion Symbology.MAXICODE
        // region Symbology.MICROPDF417
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.MICROPDF417_ENABLE};
        holder.mParaKeys = new String[]{"MICROPDF417_ENABLE"};
        mBarcodeMap.put(Symbology.MICROPDF417 + "", holder);
        // endregion Symbology.MICROPDF417
        // region Symbology.MSI
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mBarcodeLength1 = new EditTextPreference(this);
        holder.mBarcodeLength2 = new EditTextPreference(this);
        holder.mBarcodeSecondChecksum = new CheckBoxPreference(this);
        holder.mBarcodeSendCheck = new CheckBoxPreference(this);
        holder.mBarcodeSecondChecksumMode = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.MSI_ENABLE, PropertyID.MSI_LENGTH1, PropertyID.MSI_LENGTH2, PropertyID.MSI_REQUIRE_2_CHECK, PropertyID.MSI_SEND_CHECK, PropertyID.MSI_CHECK_2_MOD_11};
        holder.mParaKeys = new String[]{"MSI_ENABLE", "MSI_LENGTH1", "MSI_LENGTH2", "MSI_REQUIRE_2_CHECK", "MSI_SEND_CHECK", "MSI_CHECK_2_MOD_11"};
        mBarcodeMap.put(Symbology.MSI + "", holder);
        // endregion Symbology.MSI
        // region Symbology.PDF417
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.PDF417_ENABLE};
        holder.mParaKeys = new String[]{"PDF417_ENABLE"};
        mBarcodeMap.put(Symbology.PDF417 + "", holder);
        // endregion Symbology.PDF417
        // region Symbology.QRCODE
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.QRCODE_ENABLE};
        holder.mParaKeys = new String[]{"QRCODE_ENABLE"};
        mBarcodeMap.put(Symbology.QRCODE + "", holder);
        // endregion Symbology.QRCODE
        // region Symbology.TRIOPTIC
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.TRIOPTIC_ENABLE};
        holder.mParaKeys = new String[]{"TRIOPTIC_ENABLE"};
        mBarcodeMap.put(Symbology.TRIOPTIC + "", holder);
        // endregion Symbology.TRIOPTIC
        // region Symbology.UPCA
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mBarcodeChecksum = new CheckBoxPreference(this);
        holder.mBarcodeSystemDigit = new CheckBoxPreference(this);
        holder.mBarcodeConvertEAN13 = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.UPCA_ENABLE, PropertyID.UPCA_SEND_CHECK, PropertyID.UPCA_SEND_SYS, PropertyID.UPCA_TO_EAN13};
        holder.mParaKeys = new String[]{"UPCA_ENABLE", "UPCA_SEND_CHECK", "UPCA_SEND_SYS", "UPCA_TO_EAN13"};
        mBarcodeMap.put(Symbology.UPCA + "", holder);
        // endregion Symbology.UPCA
        // region Symbology.UPCE
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mBarcodeChecksum = new CheckBoxPreference(this);
        holder.mBarcodeSystemDigit = new CheckBoxPreference(this);
        holder.mBarcodeConvertUPCA = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.UPCE_ENABLE, PropertyID.UPCE_SEND_CHECK, PropertyID.UPCE_SEND_SYS, PropertyID.UPCE_TO_UPCA};
        holder.mParaKeys = new String[]{"UPCE_ENABLE", "UPCE_SEND_CHECK", "UPCE_SEND_SYS", "UPCE_TO_UPCA"};
        mBarcodeMap.put(Symbology.UPCE + "", holder);
        // endregion Symbology.UPCE
        // region Symbology.UPCE1
        holder = new BarcodeHolder();
        holder.mBarcodeEnable = new CheckBoxPreference(this);
        holder.mParaIds = new int[]{PropertyID.UPCE1_ENABLE};
        holder.mParaKeys = new String[]{"UPCE1_ENABLE"};
        mBarcodeMap.put(Symbology.UPCE1 + "", holder);
        // endregion Symbology.UPCE1
    }

    /**
     * ScanManager.enableSymbology
     */
    boolean enableSymbology(Symbology symbology, boolean enable) {
        boolean result = false;
        boolean isSupportBarcode = mScanManager.isSymbologySupported(symbology);
        if (isSupportBarcode) {
            boolean isEnableBarcode = mScanManager.isSymbologyEnabled(symbology);
            if (!isEnableBarcode) {
                mScanManager.enableSymbology(symbology, enable);
                result = true;
            } else {
                result = isEnableBarcode;
                Timber.i("enableSymbology , ignore " + symbology + " barcode is enable.");
            }
        } else {
            Timber.i("enableSymbology , ignore " + symbology + " barcode not Support.");
        }
        return result;
    }
}