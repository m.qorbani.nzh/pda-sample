package mortezaqn.scanManager;

import static mortezaqn.scanManager.Constants.ACTION_CAPTURE_IMAGE;
import static mortezaqn.scanManager.Constants.MSG_SHOW_SCAN_IMAGE;
import static mortezaqn.scanManager.Constants.MSG_SHOW_SCAN_RESULT;
import static mortezaqn.scanManager.Utils.bytesToHexString;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.device.ScanManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;

import timber.log.Timber;

public class ScanReceiver extends BroadcastReceiver {

    private static final String ACTION_DECODE_IMAGE_REQUEST = "action.scanner_capture_image";
    private static final String DECODE_CAPTURE_IMAGE_KEY = "bitmapBytes";
    private final Handler mHandler;

    public ScanReceiver(Handler mHandler) {
        this.mHandler = mHandler;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        Timber.i("onReceive , action:%s", action);
        // Get Scan Image . Make sure to make a request before getting a scanned image
        if (ACTION_CAPTURE_IMAGE.equals(action)) {
            byte[] imagedata = intent.getByteArrayExtra(DECODE_CAPTURE_IMAGE_KEY);
            if (imagedata != null && imagedata.length > 0) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(imagedata, 0, imagedata.length);
                Message msg = mHandler.obtainMessage(MSG_SHOW_SCAN_IMAGE);
                msg.obj = bitmap;
                mHandler.sendMessage(msg);
            } else {
                Timber.i("onReceive , ignore imagedata:%s", (Object) imagedata);
            }
        } else {
            // Get scan results, including string and byte data etc.
            byte[] barcode = intent.getByteArrayExtra(ScanManager.DECODE_DATA_TAG);
            int barcodeLen = intent.getIntExtra(ScanManager.BARCODE_LENGTH_TAG, 0);
            byte temp = intent.getByteExtra(ScanManager.BARCODE_TYPE_TAG, (byte) 0);
            String barcodeStr = intent.getStringExtra(ScanManager.BARCODE_STRING_TAG);
            //            if (ScanManagerDemo.mScanCaptureImageShow) {
            // Request images of this scan
            //                context.sendBroadcast(new Intent(ACTION_DECODE_IMAGE_REQUEST));
            //            }
            Timber.i("barcode type:%s", temp);
            String scanResult = new String(barcode, 0, barcodeLen);
            // print scan results.
            scanResult =
                    " length：" + barcodeLen +
                            "\nbarcode：" + scanResult +
                            "\nbytesToHexString：" + bytesToHexString(barcode) +
                            "\nbarcodeStr:" + barcodeStr;

            Message msg = mHandler.obtainMessage(MSG_SHOW_SCAN_RESULT);
            msg.obj = scanResult;
            mHandler.sendMessage(msg);

        }
    }
}