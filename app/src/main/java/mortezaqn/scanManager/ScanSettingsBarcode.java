package mortezaqn.scanManager;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;

import com.mortezaqn.samplescannerurovo.R;

import java.util.Map;

import timber.log.Timber;

/**
 * ScanSettingsBarcode helper
 */
public class ScanSettingsBarcode extends PreferenceFragment implements Preference.OnPreferenceChangeListener {
    private final CharSequence[] entries = new CharSequence[]{"Two check digits", "One check digits", "Two check digits and stripped", "One check digits and stripped"};
    private final CharSequence[] entryValues = new CharSequence[]{"0", "1", "2", "3"};
    private PreferenceScreen root = null;
    private ScanManagerDemo mScanDemo = null;
    private String mBarcodeKey = null;
    private Map<String, BarcodeHolder> mBarcodeMap;

    void setBarcodeMap(Map<String, BarcodeHolder> mBarcodeMap) {
        this.mBarcodeMap = mBarcodeMap;
    }

    void setScanManagerDemo(ScanManagerDemo demo, String key) {
        mScanDemo = demo;
        mBarcodeKey = key;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.scan_settings_pro);
        Timber.d("onCreate , Barcode ,root:%s", root);
    }

    @Override
    public void onStart() {
        super.onStart();
        Timber.d("onStart , Barcode ");
    }

    @Override
    public void onResume() {
        Timber.d("onResume , Barcode ");
        super.onResume();
        initSymbology();
    }

    @Override
    public void onStop() {
        super.onStop();
        Timber.d("onStop , Barcode ");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Timber.d("onDestroyView , Barcode ");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        root = this.getPreferenceScreen();
        if (root != null) {
            root.removeAll();
        }
        Timber.d("onDestroy , Barcode ,root:%s", root);
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        String key = preference.getKey();
        Timber.d("onPreferenceTreeClick preference:" + preference + ",key:" + key);
        if (preference instanceof EditTextPreference) {
            EditTextPreference editTextPreference = (EditTextPreference) findPreference(key);
            if (editTextPreference != null) {
                editTextPreference.getEditText().setText(editTextPreference.getSummary());
            }
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String key = preference.getKey();
        Timber.d("onPreferenceChange ,preference:" + preference + ",key:" + key + ",mBarcodeKey:" + mBarcodeKey + ",newValue:" + newValue);
        if (preference instanceof CheckBoxPreference) {
            boolean value = (Boolean) newValue;
            CheckBoxPreference checkBox = (CheckBoxPreference) findPreference(key);
            checkBox.setChecked(value);
            updateParameter(key, newValue);
        } else if (preference instanceof EditTextPreference) {
            String value = (String) newValue;
            EditTextPreference editTextPreference = (EditTextPreference) findPreference(key);
            if (editTextPreference != null) {
                editTextPreference.setSummary(value);
            }
            updateParameter(key, newValue);
        } else if (preference instanceof ListPreference) {
            String value = (String) newValue;
            int val = Integer.parseInt(value);
            ListPreference listPreference = (ListPreference) findPreference(key);
            if (listPreference != null) {
                listPreference.setValue(value);
                listPreference.setSummary(entries[val]);
            }
            updateParameter(key, newValue);
            Timber.d("onPreferenceChange ------------ preference:ListPreference");
        }
        return false;
    }

    /**
     * Use mBarcodeMap, key:Symbology enums toString , value:BarcodeHolder class
     */
    private void initSymbology() {
        Timber.d("initSymbology , Barcode mBarcodeKey:%s", mBarcodeKey);
        if (mBarcodeKey != null) {
            BarcodeHolder barcodeHolder = mBarcodeMap.get(mBarcodeKey);
            if (barcodeHolder != null) {
                // barcodeHolder.mParaIds are PropertyID Attributes ,
                // Example: PropertyID.QRCODE_ENABLE/PropertyID.EAN13_ENABLE/PropertyID.CODE128_ENABLE etc.
                int[] values = mScanDemo.mScanManager.getParameterInts(barcodeHolder.mParaIds);
                int valuesLength = 0;
                if (values != null) {
                    valuesLength = values.length;
                }
                if (barcodeHolder.mBarcodeEnable == null || valuesLength <= 0) {
                    Timber.d("initSymbology , ignore barcode enable:" + barcodeHolder.mBarcodeEnable + ",para value:" + valuesLength);
                    return;
                }
                int indexCount = valuesLength;
                Timber.d("initSymbology , Barcode initSymbology ,indexCount:%s", indexCount);
                if (barcodeHolder.mBarcodeEnable != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeEnable.setTitle(mBarcodeKey);
                    barcodeHolder.mBarcodeEnable.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeEnable.setSummary(mBarcodeKey);
                    barcodeHolder.mBarcodeEnable.setChecked(values[valuesLength - indexCount] == 1);
                    barcodeHolder.mBarcodeEnable.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeEnable);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeLength1 != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeLength1.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeLength1.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeLength1.setSummary(values[valuesLength - indexCount] + "");
                    barcodeHolder.mBarcodeLength1.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeLength1);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeLength2 != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeLength2.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeLength2.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeLength2.setSummary(values[valuesLength - indexCount] + "");
                    barcodeHolder.mBarcodeLength2.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeLength2);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeNOTIS != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeNOTIS.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeNOTIS.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeNOTIS.setSummary(mBarcodeKey + " NOTIS");
                    barcodeHolder.mBarcodeNOTIS.setChecked(values[valuesLength - indexCount] == 1);
                    barcodeHolder.mBarcodeNOTIS.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeNOTIS);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeCLSI != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeCLSI.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeCLSI.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeCLSI.setSummary(mBarcodeKey + " CLSI");
                    barcodeHolder.mBarcodeCLSI.setChecked(values[valuesLength - indexCount] == 1);
                    barcodeHolder.mBarcodeCLSI.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeCLSI);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeISBT != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeISBT.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeISBT.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeISBT.setSummary(mBarcodeKey + " CLSI128");
                    barcodeHolder.mBarcodeISBT.setChecked(values[valuesLength - indexCount] == 1);
                    barcodeHolder.mBarcodeISBT.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeISBT);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeChecksum != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeChecksum.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeChecksum.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeChecksum.setSummary(mBarcodeKey + " Checksum");
                    barcodeHolder.mBarcodeChecksum.setChecked(values[valuesLength - indexCount] == 1);
                    barcodeHolder.mBarcodeChecksum.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeChecksum);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeSendCheck != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeSendCheck.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeSendCheck.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeSendCheck.setSummary(mBarcodeKey + " SendCheck");
                    barcodeHolder.mBarcodeSendCheck.setChecked(values[valuesLength - indexCount] == 1);
                    barcodeHolder.mBarcodeSendCheck.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeSendCheck);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeFullASCII != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeFullASCII.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeFullASCII.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeFullASCII.setSummary(mBarcodeKey + " Full ASCII");
                    barcodeHolder.mBarcodeFullASCII.setChecked(values[valuesLength - indexCount] == 1);
                    barcodeHolder.mBarcodeFullASCII.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeFullASCII);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeCheckDigit != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeCheckDigit.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeCheckDigit.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeCheckDigit.setEntries(entries);
                    barcodeHolder.mBarcodeCheckDigit.setEntryValues(entryValues);
                    barcodeHolder.mBarcodeCheckDigit.setValue(entryValues[values[valuesLength - indexCount]].toString());
                    barcodeHolder.mBarcodeCheckDigit.setSummary(entries[values[valuesLength - indexCount]]);
                    barcodeHolder.mBarcodeCheckDigit.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeCheckDigit);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeBookland != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeBookland.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeBookland.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeBookland.setSummary(mBarcodeKey + " Bookland");
                    barcodeHolder.mBarcodeBookland.setChecked(values[valuesLength - indexCount] == 1);
                    barcodeHolder.mBarcodeBookland.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeBookland);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeSecondChecksum != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeSecondChecksum.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeSecondChecksum.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeSecondChecksum.setSummary(mBarcodeKey + " Second Checksum");
                    barcodeHolder.mBarcodeSecondChecksum.setChecked(values[valuesLength - indexCount] == 1);
                    barcodeHolder.mBarcodeSecondChecksum.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeSecondChecksum);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeSecondChecksumMode != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeSecondChecksumMode.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeSecondChecksumMode.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeSecondChecksumMode.setSummary(mBarcodeKey + " Second Checksum Mode 11");
                    barcodeHolder.mBarcodeSecondChecksumMode.setChecked(values[valuesLength - indexCount] == 1);
                    barcodeHolder.mBarcodeSecondChecksumMode.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeSecondChecksumMode);
                    indexCount--;
                }
                // PostalCode
                    /*if(barcodeHolder.mBarcodePostalCode!=null && (indexCount > 0)) {
                        barcodeHolder.mBarcodePostalCode.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                        barcodeHolder.mBarcodePostalCode.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                        barcodeHolder.mBarcodePostalCode.setSummary(values[valuesLength - indexCount]);
                        barcodeHolder.mBarcodePostalCode.setOnPreferenceChangeListener(this);
                        this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodePostalCode);
                        indexCount--;
                    }*/
                if (barcodeHolder.mBarcodeSystemDigit != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeSystemDigit.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeSystemDigit.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeSystemDigit.setSummary(mBarcodeKey + " System Digit");
                    barcodeHolder.mBarcodeSystemDigit.setChecked(values[valuesLength - indexCount] == 1);
                    barcodeHolder.mBarcodeSystemDigit.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeSystemDigit);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeConvertEAN13 != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeConvertEAN13.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeConvertEAN13.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeConvertEAN13.setSummary(mBarcodeKey + " Convert to EAN13");
                    barcodeHolder.mBarcodeConvertEAN13.setChecked(values[valuesLength - indexCount] == 1);
                    barcodeHolder.mBarcodeConvertEAN13.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeConvertEAN13);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeConvertUPCA != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeConvertUPCA.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeConvertUPCA.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeConvertUPCA.setSummary(mBarcodeKey + " Convert to UPCA");
                    barcodeHolder.mBarcodeConvertUPCA.setChecked(values[valuesLength - indexCount] == 1);
                    barcodeHolder.mBarcodeConvertUPCA.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeConvertUPCA);
                    indexCount--;
                }
                if (barcodeHolder.mBarcodeEanble25DigitExtensions != null && (indexCount > 0)) {
                    barcodeHolder.mBarcodeEanble25DigitExtensions.setTitle(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeEanble25DigitExtensions.setKey(barcodeHolder.mParaKeys[valuesLength - indexCount]);
                    barcodeHolder.mBarcodeEanble25DigitExtensions.setSummary(mBarcodeKey + " Enable 2-5 Digit Extensions");
                    barcodeHolder.mBarcodeEanble25DigitExtensions.setChecked(values[valuesLength - indexCount] == 1);
                    barcodeHolder.mBarcodeEanble25DigitExtensions.setOnPreferenceChangeListener(this);
                    this.getPreferenceScreen().addPreference(barcodeHolder.mBarcodeEanble25DigitExtensions);
                    indexCount--;
                }
            }
        }
    }

    private void updateParameter(String key, Object obj) {
        if (mBarcodeKey != null) {
            BarcodeHolder barcodeHolder = mBarcodeMap.get(mBarcodeKey);
            if (barcodeHolder != null) {
                if (barcodeHolder.mParaKeys != null && barcodeHolder.mParaKeys.length > 0) {
                    int index = 0;
                    for (int i = 0; i < barcodeHolder.mParaKeys.length; i++) {
                        if (key.equals(barcodeHolder.mParaKeys[i])) {
                            Timber.d("onPreferenceChange , index:" + index + ",key:" + key);
                            break;
                        }
                        index++;
                    }
                    int[] idBuff = new int[]{barcodeHolder.mParaIds[index]};
                    int[] valueBuff = new int[1];
                    if (obj instanceof Boolean) {
                        boolean value = (boolean) obj;
                        valueBuff[0] = value ? 1 : 0;
                    } else if (obj instanceof String) {
                        int value = Integer.parseInt((String) obj);
                        valueBuff[0] = value;
                    }
                    mScanDemo.mScanManager.setParameterInts(idBuff, valueBuff);
                }
            }
        }
    }


}