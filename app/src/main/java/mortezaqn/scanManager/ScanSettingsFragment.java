package mortezaqn.scanManager;


import static mortezaqn.scanManager.Constants.DECODE_CAPTURE_IMAGE_SHOW;
import static mortezaqn.scanManager.Constants.DECODE_ENABLE;
import static mortezaqn.scanManager.Constants.SCAN_BARCODE_SYMBOLOGY_LIST_KEY;
import static mortezaqn.scanManager.Constants.SCAN_CAPTURE_IMAGE;
import static mortezaqn.scanManager.Constants.SCAN_CONTINUOUS;
import static mortezaqn.scanManager.Constants.SCAN_HOST;
import static mortezaqn.scanManager.Constants.SCAN_INTENT_ACTION;
import static mortezaqn.scanManager.Constants.SCAN_INTENT_BARCODE_TYPE;
import static mortezaqn.scanManager.Constants.SCAN_INTENT_LABEL;
import static mortezaqn.scanManager.Constants.SCAN_KEYBOARD_OUTPUT_TYPE;
import static mortezaqn.scanManager.Constants.SCAN_OUTPUT_MODE;
import static mortezaqn.scanManager.Constants.SCAN_PULSE;
import static mortezaqn.scanManager.Constants.SCAN_RESET;
import static mortezaqn.scanManager.Constants.SCAN_SERVICE_ONOFF;
import static mortezaqn.scanManager.Constants.SCAN_SOUND_MODE;
import static mortezaqn.scanManager.Constants.SCAN_TRIGGER_LOCK;
import static mortezaqn.scanManager.Constants.SCAN_TRIGGER_MODE;

import android.device.scanner.configuration.PropertyID;
import android.device.scanner.configuration.Triggering;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.widget.Toast;

import com.mortezaqn.samplescannerurovo.R;

import timber.log.Timber;

/**
 * Menu helper
 */
public class ScanSettingsFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener {

    private SwitchPreference mScanServiceOnOff = null;
    private CheckBoxPreference mCheckBoxScanTriggerLock = null;
    private CheckBoxPreference mCheckBoxScanCaptureImage = null;
    private ListPreference mScanTriggerMode = null;
    private ListPreference mScanOutputMode = null;
    private ListPreference mScanKeyboardOutputType = null;
    private Preference mScanReset = null;
    private ListPreference mScanSendSounds = null;
    private EditTextPreference mIntentAction = null;
    private EditTextPreference mIntentLabel = null;
    private EditTextPreference mIntentBarcodeType = null;
    private Preference mBarcodeSymbologyList = null;
    private ScanManagerDemo mScanDemo = null;

    void setScanManagerDemo(ScanManagerDemo demo) {mScanDemo = demo;}

    /**
     * ScanManager.getParameterString
     */
    private String[] getParameterString(int[] ids) {
        return mScanDemo.mScanManager.getParameterString(ids);
    }

    void resetScan() {
        mScanServiceOnOff.setChecked(true);
        mScanDemo.setScanEnable(true);
        Timber.d("resetScan , :");
        mScanDemo.updateScanShared(DECODE_ENABLE, true);
        mScanDemo.openScanner();

        mCheckBoxScanTriggerLock.setChecked(false);
        mScanDemo.updateLockTriggerState(false);

        mScanTriggerMode.setValue(SCAN_HOST);
        mScanTriggerMode.setSummary(mScanTriggerMode.getEntry());
        mScanDemo.setTrigger(Triggering.HOST);

        mScanOutputMode.setValue(mScanDemo.DECODE_OUTPUT_MODE_FOCUS + "");
        mScanOutputMode.setSummary(mScanOutputMode.getEntry());
        mScanDemo.setScanOutputMode(mScanDemo.DECODE_OUTPUT_MODE_FOCUS);

        int[] id = new int[]{PropertyID.WEDGE_KEYBOARD_TYPE};
        int[] value = getParameterInts(id);
        mScanKeyboardOutputType.setValue(value[0] + "");
        mScanKeyboardOutputType.setSummary(mScanKeyboardOutputType.getEntry());

        id = new int[]{PropertyID.GOOD_READ_BEEP_ENABLE};
        int[] valueBuff = new int[]{1};
        setParameterInts(id, valueBuff);
        mScanSendSounds.setValue(valueBuff[0] + "");
        mScanSendSounds.setSummary(mScanSendSounds.getEntry());

        id = new int[]{PropertyID.WEDGE_INTENT_ACTION_NAME, PropertyID.WEDGE_INTENT_DATA_STRING_TAG, PropertyID.WEDGE_INTENT_LABEL_TYPE_TAG};
        String[] valueBuffStr = new String[]{"android.intent.ACTION_DECODE_DATA", "barcode_string", "barcodeType"};
        mIntentAction.setSummary(valueBuffStr[0]);
        mIntentLabel.setSummary(valueBuffStr[1]);
        mIntentBarcodeType.setSummary(valueBuffStr[2]);
        setParameterString(id, valueBuffStr);

        mScanDemo.setScanCaptureImageShow(false);
        mCheckBoxScanCaptureImage.setChecked(false);
        mScanDemo.updateScanShared(DECODE_CAPTURE_IMAGE_SHOW, false);
        mScanDemo.updateCaptureImage();
    }

    /**
     * ScanManager.getParameterInts
     */
    private int[] getParameterInts(int[] ids) {
        return mScanDemo.mScanManager.getParameterInts(ids);
    }

    /**
     * ScanManager.setParameterInts
     */
    private void setParameterInts(int[] ids, int[] values) {
        mScanDemo.mScanManager.setParameterInts(ids, values);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceScreen root = this.getPreferenceScreen();
        if (root != null) {
            root.removeAll();
        }
        addPreferencesFromResource(R.xml.scan_settings);
        // region bind views
        mScanServiceOnOff = (SwitchPreference) findPreference(SCAN_SERVICE_ONOFF);
        mScanServiceOnOff.setOnPreferenceChangeListener(this);
        mCheckBoxScanTriggerLock = (CheckBoxPreference) findPreference(SCAN_TRIGGER_LOCK);
        mCheckBoxScanTriggerLock.setOnPreferenceChangeListener(this);
        mScanTriggerMode = (ListPreference) findPreference(SCAN_TRIGGER_MODE);
        mScanTriggerMode.setOnPreferenceChangeListener(this);
        mScanOutputMode = (ListPreference) findPreference(SCAN_OUTPUT_MODE);
        mScanOutputMode.setOnPreferenceChangeListener(this);
        mScanKeyboardOutputType = (ListPreference) findPreference(SCAN_KEYBOARD_OUTPUT_TYPE);
        mScanKeyboardOutputType.setOnPreferenceChangeListener(this);
        mScanSendSounds = (ListPreference) findPreference(SCAN_SOUND_MODE);
        mScanSendSounds.setOnPreferenceChangeListener(this);
        mIntentAction = (EditTextPreference) findPreference(SCAN_INTENT_ACTION);
        mIntentAction.setOnPreferenceChangeListener(this);
        mIntentLabel = (EditTextPreference) findPreference(SCAN_INTENT_LABEL);
        mIntentLabel.setOnPreferenceChangeListener(this);
        mIntentBarcodeType = (EditTextPreference) findPreference(SCAN_INTENT_BARCODE_TYPE);
        mIntentBarcodeType.setOnPreferenceChangeListener(this);
        mCheckBoxScanCaptureImage = (CheckBoxPreference) findPreference(SCAN_CAPTURE_IMAGE);
        mCheckBoxScanCaptureImage.setOnPreferenceChangeListener(this);
        mBarcodeSymbologyList = findPreference(SCAN_BARCODE_SYMBOLOGY_LIST_KEY);
        mBarcodeSymbologyList.setOnPreferenceChangeListener(this);
        // endregion bind views
        mScanReset = findPreference(SCAN_RESET);
        initFragment();
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        Timber.d("onPreferenceTreeClick preference:%s", preference);
        if (mScanReset == preference) {
            Timber.d("onPreferenceTreeClick mScanReset:%s", mScanReset);
            mScanDemo.resetScanner();
        } else if (preference == mIntentAction) {
            mIntentAction.getEditText().setText(mIntentAction.getSummary());
        } else if (preference == mIntentLabel) {
            mIntentLabel.getEditText().setText(mIntentLabel.getSummary());
        } else if (preference == mBarcodeSymbologyList) {
            Timber.d("onPreferenceChange scanSettingsBarcodeList()");
            mScanDemo.scanSettingsBarcodeList();
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String key = preference.getKey();
        Timber.d("onPreferenceChange preference:" + preference + ",newValue" + newValue + ",key:" + key);
        switch (key) {
            case SCAN_SERVICE_ONOFF: {
                boolean value = (Boolean) newValue;
                if (mScanServiceOnOff.isChecked() != value) {
                    mScanServiceOnOff.setChecked(value);
                    mScanDemo.setScanEnable(value);
                    Timber.d("initView , Switch:%s", value);
                    mScanDemo.updateScanShared(DECODE_ENABLE, value);
                    if (value) {
                        mScanDemo.openScanner();
                    } else {
                        mScanDemo.closeScanner();
                    }
                }
                Timber.d("onPreferenceChange mScanServiceOnOff preference:" + preference + ",newValue:" + newValue + ",value:" + value);
                break;
            }
            case SCAN_TRIGGER_LOCK: {
                boolean value = (Boolean) newValue;
                Timber.d("onPreferenceChange mScanServiceOnOff preference:" + preference + ",newValue:" + newValue);
                mCheckBoxScanTriggerLock.setChecked(value);
                mScanDemo.updateLockTriggerState(value);
                break;
            }
            case SCAN_TRIGGER_MODE: {
                String mode = (String) newValue;
                Timber.d("onPreferenceChange mScanServiceOnOff2 preference:" + preference + ",newValue:" + newValue);
                if (SCAN_HOST.equals(mode)) {
                    mScanTriggerMode.setValue(SCAN_HOST);
                    mScanTriggerMode.setSummary(mScanTriggerMode.getEntry());
                    mScanDemo.setTrigger(Triggering.HOST);
                } else if (SCAN_CONTINUOUS.equals(mode)) {
                    mScanTriggerMode.setValue(SCAN_CONTINUOUS);
                    mScanTriggerMode.setSummary(mScanTriggerMode.getEntry());
                    mScanDemo.setTrigger(Triggering.CONTINUOUS);
                } else if (SCAN_PULSE.equals(mode)) {
                    mScanTriggerMode.setValue(SCAN_PULSE);
                    mScanTriggerMode.setSummary(mScanTriggerMode.getEntry());
                    mScanDemo.setTrigger(Triggering.PULSE);
                }
                break;
            }
            case SCAN_OUTPUT_MODE: {
                String mode = (String) newValue;
                int outputMode = mode != null ? Integer.parseInt(mode) : mScanDemo.DECODE_OUTPUT_MODE_FOCUS;
                Timber.d("onPreferenceChange SCAN_OUTPUT_MODE preference:" + preference + ",mode:" + mode);
                mScanOutputMode.setValue(mode);
                mScanOutputMode.setSummary(mScanOutputMode.getEntry());
                mScanDemo.setScanOutputMode(outputMode);
                break;
            }
            case SCAN_KEYBOARD_OUTPUT_TYPE: {
                int value = Integer.parseInt((String) newValue);
                int[] id = new int[]{PropertyID.WEDGE_KEYBOARD_TYPE};
                int[] values = getParameterInts(id);
                if (values[0] != value) {
                    mScanKeyboardOutputType.setValue(value + "");
                    mScanKeyboardOutputType.setSummary(mScanKeyboardOutputType.getEntry());
                    int[] valueBuff = new int[]{value};
                    setParameterInts(id, valueBuff);
                }
                Timber.d("onPreferenceChange SCAN_KEYBOARD_OUTPUT_TYPE value:" + value + ",values[0]:" + values[0]);
                break;
            }
            case SCAN_SOUND_MODE: {
                int value = Integer.parseInt((String) newValue);
                int[] id = new int[]{PropertyID.GOOD_READ_BEEP_ENABLE};
                int[] values = getParameterInts(id);
                if (values[0] != value) {
                    mScanSendSounds.setValue(value + "");
                    mScanSendSounds.setSummary(mScanSendSounds.getEntry());
                    int[] valueBuff = new int[]{value};
                    setParameterInts(id, valueBuff);
                }
                Timber.d("onPreferenceChange SCAN_SOUND_MODE preference:" + preference + ",newValue:" + newValue);
                break;
            }
            case SCAN_INTENT_ACTION: {
                String newAction = (String) newValue;
                int[] id = new int[]{PropertyID.WEDGE_INTENT_ACTION_NAME};
                String[] value = getParameterString(id);
                if (newAction != null && !newAction.equals(value[0])) {
                    mIntentAction.setSummary(newAction);
                    String[] valueBuff = new String[]{newAction};
                    setParameterString(id, valueBuff);
                }
                break;
            }
            case SCAN_INTENT_LABEL: {
                String newAction = (String) newValue;
                int[] id = new int[]{PropertyID.WEDGE_INTENT_DATA_STRING_TAG};
                String[] value = getParameterString(id);
                if (newAction != null && !newAction.equals(value[0])) {
                    mIntentLabel.setSummary(newAction);
                    String[] valueBuff = new String[]{newAction};
                    setParameterString(id, valueBuff);
                }
                break;
            }
            case SCAN_INTENT_BARCODE_TYPE: {
                String newAction = (String) newValue;
                int[] id = new int[]{PropertyID.WEDGE_INTENT_LABEL_TYPE_TAG};
                String[] value = getParameterString(id);
                if (newAction != null && !newAction.equals(value[0])) {
                    mIntentLabel.setSummary(newAction);
                    String[] valueBuff = new String[]{newAction};
                    setParameterString(id, valueBuff);
                }
                break;
            }
            case SCAN_CAPTURE_IMAGE: {
                boolean value = (Boolean) newValue;
                int outputMode = mScanDemo.mScanManager.getOutputMode();
                if (value && outputMode != mScanDemo.DECODE_OUTPUT_MODE_INTENT) {
                    Toast.makeText(mScanDemo, R.string.scan_cupture_image_prompt, Toast.LENGTH_LONG).show();
                }
                mScanDemo.setScanCaptureImageShow(value);
                Timber.d("onPreferenceChange SCAN_CAPTURE_IMAGE preference:" + preference + ",newValue:" + newValue);
                mCheckBoxScanCaptureImage.setChecked(value);
                mScanDemo.updateScanShared(DECODE_CAPTURE_IMAGE_SHOW, value);
                mScanDemo.updateCaptureImage();
                break;
            }
        }
        return false;
    }

    private void initFragment() {
        Triggering mode = mScanDemo.mScanManager.getTriggerMode();
        switch (mode) {
            case HOST:
                mScanTriggerMode.setValue(SCAN_HOST);
                break;
            case CONTINUOUS:
                mScanTriggerMode.setValue(SCAN_CONTINUOUS);
                break;
            case PULSE:
                mScanTriggerMode.setValue(SCAN_PULSE);
                break;
        }
        mScanTriggerMode.setSummary(mScanTriggerMode.getEntry());

        int outputMode = mScanDemo.mScanManager.getOutputMode();
        mScanOutputMode.setValue(outputMode + "");
        mScanOutputMode.setSummary(mScanOutputMode.getEntry());

        int[] id = new int[]{PropertyID.WEDGE_KEYBOARD_TYPE};
        int[] valueType = getParameterInts(id);
        mScanKeyboardOutputType.setValue(valueType[0] + "");
        mScanKeyboardOutputType.setSummary(mScanKeyboardOutputType.getEntry());

        id = new int[]{PropertyID.GOOD_READ_BEEP_ENABLE};
        int[] values = getParameterInts(id);
        mScanSendSounds.setValue(values[0] + "");
        mScanSendSounds.setSummary(mScanSendSounds.getEntry());

        id = new int[]{PropertyID.WEDGE_INTENT_ACTION_NAME, PropertyID.WEDGE_INTENT_DATA_STRING_TAG, PropertyID.WEDGE_INTENT_LABEL_TYPE_TAG};
        String[] value = getParameterString(id);
        mIntentAction.setSummary(value[0]);
        mIntentLabel.setSummary(value[1]);
        mIntentBarcodeType.setSummary(value[2]);
        mCheckBoxScanCaptureImage.setChecked(mScanDemo.getDecodeScanShared(DECODE_CAPTURE_IMAGE_SHOW));
    }

    /**
     * ScanManager.setParameterString
     */
    private boolean setParameterString(int[] ids, String[] values) {
        return mScanDemo.mScanManager.setParameterString(ids, values);
    }
}