package mortezaqn.scanManager;

import static mortezaqn.scanManager.Constants.BARCODE_SUPPORT_SYMBOLOGY;

import android.device.scanner.configuration.Symbology;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;

import com.mortezaqn.samplescannerurovo.R;

import timber.log.Timber;

/**
 * SettingsBarcodeList helper
 */
public class SettingsBarcodeList extends PreferenceFragment implements Preference.OnPreferenceChangeListener {
    private ScanManagerDemo mScanDemo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceScreen root = this.getPreferenceScreen();
        if (root != null) {
            root.removeAll();
        }
        addPreferencesFromResource(R.xml.scan_settings_pro);
        Timber.d("onCreate , ,root:%s", root);   //Symbology s = BARCODE_SUPPORT_SYMBOLOGY[9];
        initSymbology();
    }

    /**
     * Use Symbology enumeration
     */
    private void initSymbology() {
        if (mScanDemo != null) {
            int length = BARCODE_SUPPORT_SYMBOLOGY.length;
            Timber.d("initSymbology  length : %s", length);
            for (int i = 0; i < length; i++) {
                if (mScanDemo != null && isSymbologySupported(BARCODE_SUPPORT_SYMBOLOGY[i])) {
                    Preference mBarcode = new Preference(mScanDemo);
                    mBarcode.setTitle(BARCODE_SUPPORT_SYMBOLOGY[i] + "");
                    mBarcode.setKey(BARCODE_SUPPORT_SYMBOLOGY[i] + "");
                    this.getPreferenceScreen().addPreference(mBarcode);
                } else {
                    Timber.d("initSymbology , Not Support Barcode %s", BARCODE_SUPPORT_SYMBOLOGY[i]);
                }
            }
        }
    }

    private boolean isSymbologySupported(Symbology symbology) {
        boolean isSupport = false;
        if (mScanDemo.mScanManager != null) {
            isSupport = mScanDemo.mScanManager.isSymbologySupported(symbology);
        }
        return isSupport;
    }

    public void setScanManagerDemo(ScanManagerDemo demo) {
        mScanDemo = demo;
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        Timber.d("onPreferenceTreeClick preference:%s", preference);
        String key = preference.getKey();
        if (key != null) {
            mScanDemo.updateScanSettingsBarcode(key);
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        return false;
    }
}