package mortezaqn.scanManager;

public class Utils {

    /**
     * byte[] toHex String
     *
     * @param src
     * @return String
     */
    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder();
        if (src == null || src.length == 0) {return null;}
        for (byte b : src) {
            int v = b & 0xFF;
            String hexString = Integer.toHexString(v);
            if (hexString.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hexString);
        }
        return stringBuilder.toString();
    }
}