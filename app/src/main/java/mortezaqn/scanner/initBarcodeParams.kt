package mortezaqn.scanner

import android.content.Context
import android.device.scanner.configuration.PropertyID
import android.device.scanner.configuration.Symbology
import androidx.preference.CheckBoxPreference
import androidx.preference.EditTextPreference
import androidx.preference.ListPreference
import mortezaqn.common.Constants.INT_ARRAY_AZTEC
import mortezaqn.common.Constants.INT_ARRAY_CHINESE25
import mortezaqn.common.Constants.INT_ARRAY_CODABAR
import mortezaqn.common.Constants.INT_ARRAY_CODE128
import mortezaqn.common.Constants.INT_ARRAY_CODE32
import mortezaqn.common.Constants.INT_ARRAY_CODE39
import mortezaqn.common.Constants.INT_ARRAY_CODE93
import mortezaqn.common.Constants.INT_ARRAY_COMPOSITE_CC_AB
import mortezaqn.common.Constants.INT_ARRAY_COMPOSITE_CC_C
import mortezaqn.common.Constants.INT_ARRAY_DATAMATRIX
import mortezaqn.common.Constants.INT_ARRAY_DISCRETE25
import mortezaqn.common.Constants.INT_ARRAY_EAN13
import mortezaqn.common.Constants.INT_ARRAY_EAN8
import mortezaqn.common.Constants.INT_ARRAY_GS1_128
import mortezaqn.common.Constants.INT_ARRAY_GS1_14
import mortezaqn.common.Constants.INT_ARRAY_GS1_EXP
import mortezaqn.common.Constants.INT_ARRAY_GS1_LIMIT
import mortezaqn.common.Constants.INT_ARRAY_INTERLEAVED25
import mortezaqn.common.Constants.INT_ARRAY_MATRIX25
import mortezaqn.common.Constants.INT_ARRAY_MAXICODE
import mortezaqn.common.Constants.INT_ARRAY_MICROPDF417
import mortezaqn.common.Constants.INT_ARRAY_MSI
import mortezaqn.common.Constants.INT_ARRAY_PDF417
import mortezaqn.common.Constants.INT_ARRAY_QRCODE
import mortezaqn.common.Constants.INT_ARRAY_TRIOPTIC
import mortezaqn.common.Constants.INT_ARRAY_UPCA
import mortezaqn.common.Constants.INT_ARRAY_UPCE
import mortezaqn.common.Constants.INT_ARRAY_UPCE1
import mortezaqn.common.Constants.KEY_ARRAY_AZTEC
import mortezaqn.common.Constants.KEY_ARRAY_CHINESE25
import mortezaqn.common.Constants.KEY_ARRAY_CODABAR
import mortezaqn.common.Constants.KEY_ARRAY_CODE128
import mortezaqn.common.Constants.KEY_ARRAY_CODE32
import mortezaqn.common.Constants.KEY_ARRAY_CODE39
import mortezaqn.common.Constants.KEY_ARRAY_CODE93
import mortezaqn.common.Constants.KEY_ARRAY_COMPOSITE_CC_AB
import mortezaqn.common.Constants.KEY_ARRAY_COMPOSITE_CC_C
import mortezaqn.common.Constants.KEY_ARRAY_DATAMATRIX
import mortezaqn.common.Constants.KEY_ARRAY_DISCRETE25
import mortezaqn.common.Constants.KEY_ARRAY_EAN13
import mortezaqn.common.Constants.KEY_ARRAY_EAN8
import mortezaqn.common.Constants.KEY_ARRAY_GS1_128
import mortezaqn.common.Constants.KEY_ARRAY_GS1_14
import mortezaqn.common.Constants.KEY_ARRAY_GS1_EXP
import mortezaqn.common.Constants.KEY_ARRAY_GS1_LIMIT
import mortezaqn.common.Constants.KEY_ARRAY_INTERLEAVED25
import mortezaqn.common.Constants.KEY_ARRAY_MATRIX25
import mortezaqn.common.Constants.KEY_ARRAY_MAXICODE
import mortezaqn.common.Constants.KEY_ARRAY_MICROPDF417
import mortezaqn.common.Constants.KEY_ARRAY_MSI
import mortezaqn.common.Constants.KEY_ARRAY_PDF417
import mortezaqn.common.Constants.KEY_ARRAY_QRCODE
import mortezaqn.common.Constants.KEY_ARRAY_TRIOPTIC
import mortezaqn.common.Constants.KEY_ARRAY_UPCA
import mortezaqn.common.Constants.KEY_ARRAY_UPCE
import mortezaqn.common.Constants.KEY_ARRAY_UPCE1
import kotlin.collections.set

fun Context.initBarcodeParams(vararg args: Symbology): HashMap<String, BarcodeHolder> {
    val barcode = BarcodeHolder()
    val configBarcode = hashMapOf<String, BarcodeHolder>()
    configBarcode.clear()
    barcode.apply {
        mBarcodeEnable = CheckBoxPreference(this@initBarcodeParams)
        args.forEach { symbology ->
            when (symbology) {
                Symbology.AZTEC             -> {
                    mParaIds = INT_ARRAY_AZTEC
                    mParaKeys = KEY_ARRAY_AZTEC
                    configBarcode[Symbology.AZTEC.toString()] = barcode
                }

                Symbology.CHINESE25         -> {
                    mParaKeys = KEY_ARRAY_CHINESE25
                    mParaIds = INT_ARRAY_CHINESE25
                    configBarcode[Symbology.CHINESE25.toString()] = barcode
                }

                Symbology.CODABAR           -> {
                    mBarcodeLength1 = EditTextPreference(this@initBarcodeParams)
                    mBarcodeLength2 = EditTextPreference(this@initBarcodeParams)
                    mBarcodeNOTIS = CheckBoxPreference(this@initBarcodeParams)
                    mBarcodeCLSI = CheckBoxPreference(this@initBarcodeParams)
                    mParaIds = intArrayOf(
                        PropertyID.CODABAR_ENABLE,
                        PropertyID.CODABAR_LENGTH1,
                        PropertyID.CODABAR_LENGTH2,
                        PropertyID.CODABAR_NOTIS,
                        PropertyID.CODABAR_CLSI,
                    )
                    mParaKeys = arrayOf(
                        "CODABAR_ENABLE",
                        "CODABAR_LENGTH1",
                        "CODABAR_LENGTH2",
                        "CODABAR_NOTIS",
                        "CODABAR_CLSI",
                    )
                    configBarcode[Symbology.CODABAR.toString()] = barcode
                }

                Symbology.CODE11            -> {
                    mBarcodeEnable = CheckBoxPreference(this@initBarcodeParams)
                    mBarcodeLength1 = EditTextPreference(this@initBarcodeParams)
                    mBarcodeLength2 = EditTextPreference(this@initBarcodeParams)
                    mBarcodeCheckDigit = ListPreference(this@initBarcodeParams)
                    mParaIds = INT_ARRAY_CODABAR
                    mParaKeys = KEY_ARRAY_CODABAR
                    configBarcode[Symbology.CODE11.toString()] = barcode
                }

                Symbology.CODE32            -> {
                    mParaIds = INT_ARRAY_CODE32
                    mParaKeys = KEY_ARRAY_CODE32
                    configBarcode[Symbology.CODE32.toString()] = barcode
                }

                Symbology.CODE39            -> {
                    mBarcodeLength1 = EditTextPreference(this@initBarcodeParams)
                    mBarcodeLength2 = EditTextPreference(this@initBarcodeParams)
                    mBarcodeChecksum = CheckBoxPreference(this@initBarcodeParams)
                    mBarcodeSendCheck = CheckBoxPreference(this@initBarcodeParams)
                    mBarcodeFullASCII = CheckBoxPreference(this@initBarcodeParams)
                    mParaIds = INT_ARRAY_CODE39
                    mParaKeys = KEY_ARRAY_CODE39
                    configBarcode[Symbology.CODE39.toString()] = barcode
                }

                Symbology.CODE93            -> {
                    mBarcodeLength1 = EditTextPreference(this@initBarcodeParams)
                    mBarcodeLength2 = EditTextPreference(this@initBarcodeParams)
                    mParaIds = INT_ARRAY_CODE93
                    mParaKeys = KEY_ARRAY_CODE93
                    configBarcode[Symbology.CODE93.toString()] = barcode
                }

                Symbology.CODE128           -> {
                    mBarcodeLength1 = EditTextPreference(this@initBarcodeParams)
                    mBarcodeLength2 = EditTextPreference(this@initBarcodeParams)
                    mBarcodeISBT = CheckBoxPreference(this@initBarcodeParams)
                    mParaIds = INT_ARRAY_CODE128
                    mParaKeys = KEY_ARRAY_CODE128
                    configBarcode[Symbology.CODE128.toString()] = barcode
                }

                Symbology.COMPOSITE_CC_AB   -> {
                    mParaIds = INT_ARRAY_COMPOSITE_CC_AB
                    mParaKeys = KEY_ARRAY_COMPOSITE_CC_AB
                    configBarcode[Symbology.COMPOSITE_CC_AB.toString()] = barcode
                }

                Symbology.COMPOSITE_CC_C    -> {
                    mParaIds = INT_ARRAY_COMPOSITE_CC_C
                    mParaKeys = KEY_ARRAY_COMPOSITE_CC_C
                    configBarcode[Symbology.COMPOSITE_CC_C.toString()] = barcode
                }

                Symbology.DATAMATRIX        -> {
                    mParaIds = INT_ARRAY_DATAMATRIX
                    mParaKeys = KEY_ARRAY_DATAMATRIX
                    configBarcode[Symbology.DATAMATRIX.toString()] = barcode
                }

                Symbology.DISCRETE25        -> {
                    mParaIds = INT_ARRAY_DISCRETE25
                    mParaKeys = KEY_ARRAY_DISCRETE25
                    configBarcode[Symbology.DISCRETE25.toString()] = barcode
                }

                Symbology.EAN8              -> {
                    mParaIds = INT_ARRAY_EAN8
                    mParaKeys = KEY_ARRAY_EAN8
                    configBarcode[Symbology.EAN8.toString()] = barcode
                }

                Symbology.EAN13             -> {
                    mBarcodeBookLand = CheckBoxPreference(this@initBarcodeParams)
                    mParaIds = INT_ARRAY_EAN13
                    mParaKeys = KEY_ARRAY_EAN13
                    configBarcode[Symbology.EAN13.toString()] = barcode
                }

                Symbology.GS1_14            -> {
                    mParaIds = INT_ARRAY_GS1_14
                    mParaKeys = KEY_ARRAY_GS1_14
                    configBarcode[Symbology.GS1_14.toString()] = barcode
                }

                Symbology.GS1_128           -> {
                    mParaIds = INT_ARRAY_GS1_128
                    mParaKeys = KEY_ARRAY_GS1_128
                    configBarcode[Symbology.GS1_128.toString()] = barcode
                }

                Symbology.GS1_EXP           -> {
                    mBarcodeLength1 = EditTextPreference(this@initBarcodeParams)
                    mBarcodeLength2 = EditTextPreference(this@initBarcodeParams)
                    mParaIds = INT_ARRAY_GS1_EXP
                    mParaKeys = KEY_ARRAY_GS1_EXP
                    configBarcode[Symbology.GS1_EXP.toString()] = barcode
                }

                Symbology.GS1_LIMIT         -> {
                    mParaIds = INT_ARRAY_GS1_LIMIT
                    mParaKeys = KEY_ARRAY_GS1_LIMIT
                    configBarcode[Symbology.GS1_LIMIT.toString()] = barcode
                }

                Symbology.INTERLEAVED25     -> {
                    mBarcodeLength1 = EditTextPreference(this@initBarcodeParams)
                    mBarcodeLength2 = EditTextPreference(this@initBarcodeParams)
                    mBarcodeChecksum = CheckBoxPreference(this@initBarcodeParams)
                    mBarcodeSendCheck = CheckBoxPreference(this@initBarcodeParams)
                    mParaIds = INT_ARRAY_INTERLEAVED25
                    mParaKeys = KEY_ARRAY_INTERLEAVED25
                    configBarcode[Symbology.INTERLEAVED25.toString()] = barcode
                }

                Symbology.MATRIX25          -> {
                    mParaIds = INT_ARRAY_MATRIX25
                    mParaKeys = KEY_ARRAY_MATRIX25
                    configBarcode[Symbology.MATRIX25.toString()] = barcode
                }

                Symbology.MAXICODE          -> {
                    mParaIds = INT_ARRAY_MAXICODE
                    mParaKeys = KEY_ARRAY_MAXICODE
                    configBarcode[Symbology.MAXICODE.toString()] = barcode
                }

                Symbology.MICROPDF417       -> {
                    mParaIds = INT_ARRAY_MICROPDF417
                    mParaKeys = KEY_ARRAY_MICROPDF417
                    configBarcode[Symbology.MICROPDF417.toString()] = barcode
                }

                Symbology.MSI               -> {
                    mBarcodeLength1 = EditTextPreference(this@initBarcodeParams)
                    mBarcodeLength2 = EditTextPreference(this@initBarcodeParams)
                    mBarcodeSecondChecksum = CheckBoxPreference(this@initBarcodeParams)
                    mBarcodeSendCheck = CheckBoxPreference(this@initBarcodeParams)
                    mBarcodeSecondChecksumMode = CheckBoxPreference(this@initBarcodeParams)
                    mParaIds = INT_ARRAY_MSI
                    mParaKeys = KEY_ARRAY_MSI
                    configBarcode[Symbology.MSI.toString()] = barcode
                }

                Symbology.PDF417            -> {
                    mParaIds = INT_ARRAY_PDF417
                    mParaKeys = KEY_ARRAY_PDF417
                    configBarcode[Symbology.PDF417.toString()] = barcode
                }

                Symbology.QRCODE            -> {
                    mParaIds = INT_ARRAY_QRCODE
                    mParaKeys = KEY_ARRAY_QRCODE
                    configBarcode[Symbology.QRCODE.toString()] = barcode
                }

                Symbology.TRIOPTIC          -> {
                    mParaIds = INT_ARRAY_TRIOPTIC
                    mParaKeys = KEY_ARRAY_TRIOPTIC
                    configBarcode[Symbology.TRIOPTIC.toString()] = barcode
                }

                Symbology.UPCA              -> {
                    mBarcodeChecksum = CheckBoxPreference(this@initBarcodeParams)
                    mBarcodeSystemDigit = CheckBoxPreference(this@initBarcodeParams)
                    mBarcodeConvertEAN13 = CheckBoxPreference(this@initBarcodeParams)
                    mParaIds = INT_ARRAY_UPCA
                    mParaKeys = KEY_ARRAY_UPCA
                    configBarcode[Symbology.UPCA.toString()] = barcode
                }

                Symbology.UPCE              -> {
                    mBarcodeChecksum = CheckBoxPreference(this@initBarcodeParams)
                    mBarcodeSystemDigit = CheckBoxPreference(this@initBarcodeParams)
                    mBarcodeConvertUPCA = CheckBoxPreference(this@initBarcodeParams)
                    mParaIds = INT_ARRAY_UPCE
                    mParaKeys = KEY_ARRAY_UPCE
                    configBarcode[Symbology.UPCE.toString()] = barcode
                }

                Symbology.UPCE1             -> {
                    mParaIds = INT_ARRAY_UPCE1
                    mParaKeys = KEY_ARRAY_UPCE1
                    configBarcode[Symbology.UPCE1.toString()] = barcode
                }

                Symbology.NONE              -> TODO()
                Symbology.COMPOSITE_TLC_39  -> TODO()
                Symbology.ISBT128           -> TODO()
                Symbology.CODE49            -> TODO()
                Symbology.TELEPEN           -> TODO()
                Symbology.CODABLOCK_A       -> TODO()
                Symbology.CODABLOCK_F       -> TODO()
                Symbology.NEC25             -> TODO()
                Symbology.KOREA_POST        -> TODO()
                Symbology.MICROQR           -> TODO()
                Symbology.CANADA_POST       -> TODO()
                Symbology.POSTAL_PLANET     -> TODO()
                Symbology.POSTAL_POSTNET    -> TODO()
                Symbology.POSTAL_4STATE     -> TODO()
                Symbology.POSTAL_UPUFICS    -> TODO()
                Symbology.POSTAL_ROYALMAIL  -> TODO()
                Symbology.POSTAL_AUSTRALIAN -> TODO()
                Symbology.POSTAL_KIX        -> TODO()
                Symbology.POSTAL_JAPAN      -> TODO()
                Symbology.HANXIN            -> TODO()
                Symbology.DOTCODE           -> TODO()
                Symbology.POSTAL_UK         -> TODO()
                Symbology.COMPOSITE_CC_B    -> TODO()
                Symbology.MicroQR           -> TODO()
                Symbology.GRIDMATRIX        -> TODO()
                Symbology.CouponCode        -> TODO()
            }
        }
    }
    return configBarcode
}